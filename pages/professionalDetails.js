import { useState, useEffect } from "react"
import Input from "@/components/InputField"
import { SingleSelectDropdown } from "@/components/Dropdown"
import { BsPersonCircle } from 'react-icons/bs'
import FormFooter from "@/components/FormFooter"
import { useRouter } from 'next/router'
import { useMediaQuery } from 'react-responsive'
import { z, ZodError } from 'zod';
import { masterDataInstance, axiosInstance } from "@/services/ApiHandler"
import Button from "@/components/Button"
import { useSidebar } from "@/components/SidebarContext";
import LocalStorageService from "@/services/LocalStorageHandler";
import { PropagateLoader } from "react-spinners";

const localStorage = LocalStorageService.getService();

export default function ProfessionalDetails() {
    const router = useRouter();
    const { toggleDisplayPageVisibility } = useSidebar();
    const [isLoading, setIsLoading] = useState(true);
    // const [organisationList,setOrganisationList]= useState([]);
    // const [reportingToList,setReportingToList] = useState([]);
    const [managementLevelList,setManagementLevelList] = useState([])
    const [isFormSubmitted,setIsFormSubmitted] = useState(false);
    const [professionalDetails, setProfessionalDetails] = useState({
        management_level: '',
        organisation_currently_employed: '',
        organisation_website: '',
        reporting_to: '',
        job_title: '',
        number_of_direct_reports: '',
        number_of_indirect_reports:'',
        time_in_current_role: '',
        responsible_for_any_revenue_or_cost_budgets: '',
        responsible_for_any_revenue_or_cost_budgets_name:'',
        total_actual_revenue_directly_responsible_for: '',
        total_cost_center_budget_directly_responsible_for: '',
        job_description_doc: '',
        job_description_doc_url:''
    })
    const [formErrors, setFormErrors] = useState({
        organisation_currently_employed: '',
        organisation_website: '',
        reporting_to: '',
        job_title: '',
        number_of_direct_reports: '',
        number_of_indirect_reports:'',
        time_in_current_role:'',
        responsible_for_any_revenue_or_cost_budgets_name:'',
        total_actual_revenue_directly_responsible_for: '',
        total_cost_center_budget_directly_responsible_for: '',
        job_description_doc: '',
      })
      const [isDisplayMode, setIsDisplayMode] = useState(false);
      const [allPrevFormsFilled,setAllPrevFormsFilled] = useState({
        is_professional_details:'',
        is_personal_details:'',
        is_current_compensation_details_1:'',
        is_current_compensation_details_2:'',
        is_current_compensation_details_3:''
      });
      const dropdownOptions = [{ name: 'Yes', id: true }, { name: 'No', id: false }]

    const isMobile = useMediaQuery({ maxWidth: 767 }, undefined, (matches) => {
        // This callback is only executed on the client side
        return matches;
    })

    const allowedFileTypes = ['application/msword', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', 'application/pdf'];

    // Define a schema
  const userSchema = z.object({
    organisation_currently_employed: z.string().min(1,{message:'This field is required.'}),
    organisation_website: z.string().url({message:'Input should be a valid URL.'}),
    reporting_to: z.string().min(1,{message:'This field is required.'}),
    job_title: z.string().min(1,{message:'This field is required.'}),
    number_of_direct_reports: z.string().min(1,{message:'This field is required.'}),
    number_of_indirect_reports: z.string().min(1,{message:'This field is required.'}),
    time_in_current_role:z.string().min(1,{message:'This field is required.'}),
    responsible_for_any_revenue_or_cost_budgets_name: z.string().min(1),
    total_actual_revenue_directly_responsible_for:z.string().refine(value => {
        // Check if the field should be compulsory based on 'responsible_for_any_revenue_or_cost_budgets'
        if (professionalDetails.responsible_for_any_revenue_or_cost_budgets) {
          return value !== undefined && value !== null && value !== '';
        }
        // If 'responsible_for_any_revenue_or_cost_budgets' is false, consider the field as valid
        return true;
      }, { message: 'This field is required.' })
      .optional(), // Marking optional

    total_cost_center_budget_directly_responsible_for: z.string().refine(value => {
        // Check if the field should be compulsory based on 'responsible_for_any_revenue_or_cost_budgets'
        if (professionalDetails.responsible_for_any_revenue_or_cost_budgets) {
          return value !== undefined && value !== null && value !== '';
        }
        // If 'responsible_for_any_revenue_or_cost_budgets' is false, consider the field as valid
        return true;
      }, { message: 'This field is required.' })
      .optional(), // Marking optional

      job_description_doc:z.custom(value => {
        // Check if the field should be compulsory 
        if (professionalDetails.job_description_doc_url === null || professionalDetails.job_description_doc_url === '') {
          return value !== undefined && value !== null && value !== '';
        }
        // If false, consider the field as valid
        return true;
      }, { message: 'This field is required.' })
      .optional(),

})



  useEffect(() => {
    if(localStorage.getAccessToken()){

      masterDataInstance
      .get("management-level/")
      .then((res) =>{
        if(res?.data.status.code == 200){
            setManagementLevelList([...res.data.data.output])}});

    axiosInstance.get("client/").then((res) => {
        if(res?.data.status.code == 200){
      const details = res.data.data.output[0];
      if (details.is_professional_details) {
        setIsDisplayMode(true);
        setIsFormSubmitted(true);
        setProfessionalDetails({
            ...professionalDetails,
            management_level: details.management_level,
            organisation_currently_employed: details.organisation_currently_employed,
            organisation_website: details.organisation_website,
            reporting_to: details.reporting_to,
            job_title: details.job_title,
            number_of_direct_reports: details?.number_of_direct_reports?.toString(),
            number_of_indirect_reports:details?.number_of_indirect_reports?.toString(),
            time_in_current_role: details.time_in_current_role?.toString(),
            responsible_for_any_revenue_or_cost_budgets: details.responsible_for_any_revenue_or_cost_budgets,
            responsible_for_any_revenue_or_cost_budgets_name:getDisplayValue(details.responsible_for_any_revenue_or_cost_budgets),
            total_actual_revenue_directly_responsible_for : details.total_actual_revenue_directly_responsible_for?.toString(),
            total_cost_center_budget_directly_responsible_for: details.total_cost_center_budget_directly_responsible_for?.toString(),
            job_description_doc_url:details.job_description_doc_url
          });
      }

      setAllPrevFormsFilled({
        is_professional_details:details.is_professional_details,
        is_personal_details:details.is_personal_details,
        is_current_compensation_details_1:details.is_current_compensation_details_1,
        is_current_compensation_details_2:details.is_current_compensation_details_2,
        is_current_compensation_details_3:details.is_current_compensation_details_3
      })

      if(details.is_current_compensation_details_1 && details.is_current_compensation_details_2 && details.is_current_compensation_details_3
        && details.is_personal_details && details.is_professional_details){
            toggleDisplayPageVisibility(true)
        }

        setIsLoading(false)
    }
    });
} else{
    router.push('/login')
}
  }, []);

  const getDisplayValue = (data) =>{
    if(data != null){
      if(data){
        return 'Yes'
      }else{
        return 'No'
      }
    }
    return null
  }

    const validateData = () => {
        try {
            const validatedData = userSchema.parse(professionalDetails);
            setFormErrors({
                organisation_currently_employed: '',
                organisation_website: '',
                reporting_to: '',
                job_title: '',
                number_of_direct_reports: '',
                number_of_indirect_reports:'',
                time_in_current_role:'',
                responsible_for_any_revenue_or_cost_budgets_name:'',
                total_actual_revenue_directly_responsible_for: '',
                total_cost_center_budget_directly_responsible_for: '',
                job_description_doc: ''
            })
            
            submitData();
          } catch (error) {
            // console.error('Validation Error:', error.errors,professionalDetails);
            if (error instanceof ZodError) {
              // Handle Zod validation errors
              const fieldErrors = {};
              error.errors.forEach((validationError) => {
                const fieldName = validationError.path.join('.');
                // Modify the response for an empty file selection
              if (fieldName === 'job_description_doc') {
                fieldErrors['job_description_doc'] = 'No file selected.'; // Custom message for empty file selection
              }else{
                fieldErrors[fieldName] = validationError.message;
              }
              });              

              setFormErrors({ ...fieldErrors });
            }
      
          }
    }


    //TODO:check file submission

    const submitData = () =>{
        const formData = new FormData();

        formData.append("management_level",professionalDetails.management_level);
        formData.append("organisation_currently_employed",professionalDetails.organisation_currently_employed);
        formData.append("organisation_website",professionalDetails.organisation_website);
        formData.append("reporting_to",professionalDetails.reporting_to);
        formData.append("job_title",professionalDetails.job_title)
        formData.append("number_of_direct_reports",professionalDetails.number_of_direct_reports)
        formData.append("number_of_indirect_reports",professionalDetails.number_of_indirect_reports)
        formData.append("time_in_current_role",professionalDetails.time_in_current_role)
        formData.append("responsible_for_any_revenue_or_cost_budgets",professionalDetails.responsible_for_any_revenue_or_cost_budgets)
        formData.append("total_actual_revenue_directly_responsible_for",professionalDetails.total_actual_revenue_directly_responsible_for)
        formData.append("total_cost_center_budget_directly_responsible_for",professionalDetails.total_cost_center_budget_directly_responsible_for)
        formData.append("job_description_doc",professionalDetails.job_description_doc)
        formData.append("is_professional_details",true)
        axiosInstance
        .put("client/", formData)
        .then((res) => {
          if (res.data.status.code == 200) {
            if(allPrevFormsFilled.is_current_compensation_details_2 && allPrevFormsFilled.is_current_compensation_details_3 && 
                allPrevFormsFilled.is_personal_details && allPrevFormsFilled.is_current_compensation_details_1){
                    router.push('/deliveryPage');
                    toggleDisplayPageVisibility(true);
                }else{
            router.push("/currentCompensation");
                }
          }
        }).catch(error=>{
            if(error.response.status == 500){
                router.push('/500')
            }
        });
    }

    if (isLoading){
        return (
          <div className='row-span-full col-span-full flex justify-center items-center'>
            <PropagateLoader
              color='#01287A'
              loading={true}
              size={20}
              cssOverride={{top:'50vh',left:'-20px'}}
              />
          </div>
        )}
    return (
        <div className="flex flex-col">
            <div className={`py-3 gap-4 items-center px-6 flex border-b-2 rounded-md w-[90%] mx-auto border-b-primary bg-white ${isMobile ? "mt-[1rem]" : "mt-[3rem]"}`}
                style={{ boxShadow: "2px 3px 6px 0px rgba(0, 82, 255, 0.25)" }}>
                <BsPersonCircle size={40} className="text-primary" />
                <span className="flex flex-col">
                    <p className={`text-primary ${isMobile ? "text-semimedium" : "text-medium"} font-bold`}>Professional Details</p>
                    <p className="text-darkGrey text-extrasmall">Professional Details</p>
                </span>
            </div>

            {isFormSubmitted && (<div className="w-[90%] mx-auto flex justify-end mt-7 mr">{isDisplayMode ? (
        <Button variant="transparent" className={`${isMobile?'text-xs w-[10rem] h-[1.7rem]':'w-[13rem] '} `} onClick={()=>setIsDisplayMode(false)}>
          Edit Professional Details
        </Button>
      ) : (
        <Button variant="transparent" className={`${isMobile?'text-xs w-[7rem] h-[1.7rem]':'w-[12rem] '} `} onClick={()=>setIsDisplayMode(true)}>
          Cancel Edit
        </Button>
      )}</div>)}

            {/* Professional Details Form Starts here... */}
            <div className="w-[90%] mx-auto flex flex-wrap gap-[5%] mt-5">
                {/* Management Level: */}
                <SingleSelectDropdown
                    options={managementLevelList}
                    optionName='name'
                    optionID='id'
                    placeholder='Management Level'
                    setselected={(name, id) => {
                        setProfessionalDetails({
                            ...professionalDetails,
                            management_level: name, 
                        })
                    }}
                    isDisplayMode={isDisplayMode}
                    selected={professionalDetails.management_level}
                    selectedOptionId={professionalDetails.management_level}
                    className={`rounded-md ${isMobile ? 'w-full mb-6' : 'w-[47.5%] mb-[2rem]'}`}
                />

                

                {/* Organisation(required): */}
                <Input type='text'
                    className={`${isMobile ? 'w-full mb-6' : 'w-[47.5%] mb-[2rem]'}`}
                    placeholder='* Organisation currently employed'
                    value={
                        professionalDetails.organisation_currently_employed
                    }
                    isDisplayMode={isDisplayMode}
                    onChange={
                        (e) => setProfessionalDetails({
                            ...professionalDetails,
                            organisation_currently_employed: e.target.value
                        })
                    } error={formErrors.organisation_currently_employed}
                />

                {/* Website(required): */}
                <Input type='url'
                    className={`${isMobile ? 'w-full mb-6' : 'w-[47.5%] mb-[2rem]'}`}
                    placeholder="* Website of the organisation"
                    value={
                        professionalDetails.organisation_website
                    }
                    isDisplayMode={isDisplayMode}
                    onChange={
                        (e) => setProfessionalDetails({
                            ...professionalDetails,
                            organisation_website: e.target.value
                        })
                    } error={formErrors.organisation_website}
                />

                {/* Reporting to(required): */}
                <Input type='text'
                    className={`${isMobile ? 'w-full mb-6' : 'w-[47.5%] mb-[2rem]'}`}
                    placeholder='* Reporting to (title only)'
                    value={
                        professionalDetails.reporting_to
                    }
                    isDisplayMode={isDisplayMode}
                    onChange={
                        (e) => setProfessionalDetails({
                            ...professionalDetails,
                            reporting_to: e.target.value
                        })
                    } error={formErrors.reporting_to}
                />

                {/* Job Title(required): */}
                <Input type='text'
                    className={`${isMobile ? 'w-full mb-6' : 'w-[47.5%] mb-[2rem]'}`}
                    placeholder="* Job Title"
                    value={
                        professionalDetails.job_title
                    }
                    isDisplayMode={isDisplayMode}
                    onChange={
                        (e) => setProfessionalDetails({
                            ...professionalDetails,
                            job_title: e.target.value
                        })
                    } error={formErrors.job_title}
                />

                {/* Number of direct reports/reportees(required): */}
                 <Input type='number'
                    className={`${isMobile ? 'w-full mb-6' : 'w-[47.5%] mb-[2rem]'}`}
                    placeholder="* Number of Direct reports/reportees"
                    value={
                        professionalDetails.number_of_direct_reports
                    }
                    isDisplayMode={isDisplayMode}
                    onChange={
                        (e) => {if (/^\d*\.?\d*$/.test(e.target.value)) {
                            setProfessionalDetails({
                            ...professionalDetails,
                            number_of_direct_reports: e.target.value
                        })}}
                    } error={formErrors.number_of_direct_reports}
                />

                {/* Number of indirect reports/reportees(required): */}
                <Input type='number'
                    className={`${isMobile ? 'w-full mb-6' : 'w-[47.5%] mb-[2rem]'}`}
                    placeholder="* Number of Indirect reports/reportees"
                    value={
                        professionalDetails.number_of_indirect_reports
                    }
                    isDisplayMode={isDisplayMode}
                    onChange={
                        (e) => {if (/^\d*\.?\d*$/.test(e.target.value)) {setProfessionalDetails({
                            ...professionalDetails,
                            number_of_indirect_reports: e.target.value
                        })}}
                    } error={formErrors.number_of_indirect_reports}
                />

                {/* Time in current role(required): */}
                <Input type='number'
                    className={`${isMobile ? 'w-full mb-6' : 'w-[47.5%] mb-[2rem]'}`}
                    placeholder="* Time in current role (in years)"
                    value={
                        professionalDetails.time_in_current_role
                    }
                    isDisplayMode={isDisplayMode}
                    onChange={
                        (e) => {if (/^\d*\.?\d*$/.test(e.target.value)) {setProfessionalDetails({
                            ...professionalDetails,
                            time_in_current_role: e.target.value
                        })}}
                    } error={formErrors.time_in_current_role}
                />

                {/* Responsible for any revenue or cost budgets?(required): */}
               
                 <SingleSelectDropdown
                    options={dropdownOptions}
                    optionName='name'
                    optionID='id'
                    placeholder="* Responsible for any revenue or cost budgets?"
                    setselected={(name, id) => {
                        if(!id){
                            setProfessionalDetails({
                                ...professionalDetails,
                                responsible_for_any_revenue_or_cost_budgets: id,
                                responsible_for_any_revenue_or_cost_budgets_name:name,
                                total_actual_revenue_directly_responsible_for:'',
                                total_cost_center_budget_directly_responsible_for:''
                            })  
                        }else{
                        setProfessionalDetails({
                            ...professionalDetails,
                            responsible_for_any_revenue_or_cost_budgets: id,
                            responsible_for_any_revenue_or_cost_budgets_name:name
                        })
                    }
                    }}
                    isDisplayMode={isDisplayMode}
                    selected={professionalDetails.responsible_for_any_revenue_or_cost_budgets_name}
                    selectedOptionId={professionalDetails.responsible_for_any_revenue_or_cost_budgets}
                    error={formErrors.responsible_for_any_revenue_or_cost_budgets_name}
                    className={`rounded-md ${isMobile ? 'w-full mb-6' : 'w-[47.5%] mb-[2rem]'}`}
                />

                {professionalDetails.responsible_for_any_revenue_or_cost_budgets?<>
                {/* Total Revenue directly responsible for?(required): */}
                <Input type='number'
                    className={`${isMobile ? 'w-full mb-6' : 'w-[47.5%] mb-[2rem]'}`}
                    placeholder="* Total actual revenue directly responsible for? (in USD)"
                    value={
                        professionalDetails.total_actual_revenue_directly_responsible_for
                    }
                    isDisplayMode={isDisplayMode}
                    onChange={
                        (e) => {if (/^\d*\.?\d*$/.test(e.target.value)) {setProfessionalDetails({
                            ...professionalDetails,
                            total_actual_revenue_directly_responsible_for: e.target.value
                        })}}
                    } error={formErrors.total_actual_revenue_directly_responsible_for}
                />

                {/* Total cost budget directly responsible for?(required): */}
                <Input type='number'
                    className={`${isMobile ? 'w-full mb-6' : 'w-[47.5%] mb-[2rem]'}`}
                    placeholder="* Total Cost center budget directly responsible for? (in USD)"
                    value={
                        professionalDetails.total_cost_center_budget_directly_responsible_for
                    }
                    isDisplayMode={isDisplayMode}
                    onChange={
                        (e) => {if (/^\d*\.?\d*$/.test(e.target.value)) {setProfessionalDetails({
                            ...professionalDetails,
                            total_cost_center_budget_directly_responsible_for:e.target.value
                        })}}
                    } error={formErrors.total_cost_center_budget_directly_responsible_for}
                /><br/></>:<br/>}

                {/* Upload Job Description(required): */}
                <Input type='file'
                    className={`${isMobile ? 'w-full mb-6' : 'w-[47.5%] mb-[2rem]'}`}
                    placeholder="* Upload Job Description (MS Word or PDF)"
                    isDisplayMode={isDisplayMode}
                    displayValue={professionalDetails.job_description_doc_url}
                    onChange={
                        (e) => setProfessionalDetails({
                            ...professionalDetails,
                            job_description_doc: e.target.files[0]
                        })
                    } error={(formErrors.job_description_doc || formErrors['job_description_doc.type'])}
                    accept=".pdf,.doc,.docx" 
                />

                
            </div>

            {/* Component to handle Back and Next Button */}
            {!isDisplayMode?<FormFooter className={`${isMobile ? ' mb-6' : ' mb-[2rem]'} w-[90%] flex justify-end mx-auto  h-[2rem]`}
                onNextClick={() => validateData()}
                onBackClick={() => router.push('/')} />:null}

            

        </div>)
}