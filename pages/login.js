import Image from 'next/image'
import Button from '@/components/Button'
import Input from '@/components/InputField'
import { useState } from 'react'
import LocalStorageService from '@/services/LocalStorageHandler'
import { useRouter } from 'next/router'
import { accessInstance } from '@/services/ApiHandler'
import { z, ZodError } from "zod";

const localStorage = LocalStorageService.getService();

export default function Home() {
    const router = useRouter()
    const [formData, setFormData] = useState({
        username: '',
        password: ''
    })
    const [formErrors, setFormErrors] = useState({
        username: "",
        password:""
      });
    const [errorMessage,setErrorMessage]= useState(null)


     // Define a schema
  const userSchema = z.object({
    username: z.string().min(1,{message:'This field is required.'}),
    password: z.string().min(1,{message:'This field is required.'}),
  });

  
    const onSubmit = () => {
        try {
            const validatedData = userSchema.parse(formData);
            setFormErrors({
              username: "",
              password:""
            });
            accessInstance.post('login/',formData).then(res=>{
                if(res.status == 200){
                    localStorage.setAccessToken(res.data.token)
                 router.push('/');
    
                }
            }).catch(e=>{
                if(e.response.status == 401){
                    setErrorMessage('Invalid credentials. Please try again.')
                }
            })
      
            // router.push('/professionalDetails')
          } catch (error) {
            // console.error("Validation Error:", error.errors);
            if (error instanceof ZodError) {
              // Handle Zod validation errors
              const fieldErrors = {};
              error.errors.forEach((validationError) => {
                const fieldName = validationError.path.join(".");
                fieldErrors[fieldName] = validationError.message;
              });
              setFormErrors({ ...fieldErrors });
            }
          }
       
    }

    return (
        <div className='w-[100vw] h-[100vh] flex relative'>
            <div className={` w-full md:w-1/2 h-full  flex flex-col justify-center items-center`}>
                <div className='w-[80%] md:w-[60%] flex flex-col items-center'><p className={`text-primary font-bold text-large md:text-extralarge`}>Welcome Back</p>
                    <p className='text-small text-primary font-light '>Welcome back! Please enter your details</p>
                    {errorMessage?<p className="text-xs pl-1 text-red-700 mt-[1rem]">{errorMessage}</p>:null}

                    <Input type='email'
                        className="w-full h-[4rem] mt-[2rem]"
                        placeholder='Email'
                        value={formData.username}
                        error={formErrors.username}
                        noLabel
                        onChange={(e) => setFormData({ ...formData, username: e.target.value })} />
                    <Input type='password'
                        className="w-full h-[4rem]"
                        placeholder='Password'
                        value={formData.password}
                        error={formErrors.password}
                        noLabel
                        onChange={(e) => setFormData({ ...formData, password: e.target.value })} />

                    <div className='w-full flex justify-center'><Button className="w-[7rem]"
                        onClick={onSubmit}>Login</Button>

                    </div>

                </div>
                <p className='mt-8 text-small text-textGrey font-light'>Don&apos;t have an account? <span className='text-primary cursor-pointer font-semibold hover:underline'
                onClick={()=>router.push('/register')}>Sign Up</span></p>

            </div>
            <div className='w-1/2 h-full top-0 absolute hidden md:block right-0'>
                <Image
                    alt='login'
                    src="/login.png"
                    fill
                    // priority="empty"
                    sizes='50vw'
                    placeholder="blur"
                    blurDataURL={'/logo.png'}
                />
                </div> 



        </div>

    )
}
