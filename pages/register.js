import Image from "next/image";
import Button from "@/components/Button";
import Input from "@/components/InputField";
import { useState } from "react";
import { useRouter } from "next/router";
import { useMediaQuery } from 'react-responsive'
import { accessInstance } from "@/services/ApiHandler";
import { z, ZodError } from "zod";


export default function Register() {
  const router = useRouter();
  const [formData, setFormData] = useState({
    name: "",
    email: "",
    phone_number: "",
    password: "",
    confirm_password: "",
    country_code:''
  });
  const [formErrors, setFormErrors] = useState({
    name: "",
    email: "",
    phone_number: "",
    password: "",
    confirm_password: "",
    country_code:"",
    existing_user:""
  });
  const [isRegistered, setIsRegistered] = useState(false);

  const isMobile = useMediaQuery({ maxWidth: 767 }, undefined, (matches) => {
    // This callback is only executed on the client side
    return matches;
})

     // Define a schema
     const userSchema = z.object({
        name: z.string().min(1,{message:'This field is required.'}),
        email: z.string().email({message:'Input should be a valid email address.'}),
        phone_number: z.string().min(10),
        password: z.string().min(1,{message:'This field is required.'}),
        confirm_password: z.string().min(1,{message:'This field is required.'}),  
        country_code: z.string().regex(/^\+\d{1,3}$/i, {
          message: 'Country code should start with a plus sign (+) followed by 1 to 3 digits',
        })
      });

  const onSubmit = () => {
    if (formData.confirm_password === formData.password) {

    try {
        const validatedData = userSchema.parse(formData);
        setFormErrors({
            name: "",
            email: "",
            phone_number: "",
            password: "",
            confirm_password: "",
            existing_user:""
          });

        accessInstance.post("create_user/", formData).then((res) => {
            if (res.status === 201) {
              setIsRegistered(true);
            }
          }).catch(error=>{
            if(JSON.parse(error.request.response).email[0]){
              const errorMessage=JSON.parse(error.request.response).email[0];
              setFormErrors({name: "",
              email: "",
              phone_number: "",
              password: "",
              confirm_password: "",
              existing_user:errorMessage.charAt(0).toUpperCase()+errorMessage.slice(1)})
            }
        });
  
        // router.push('/professionalDetails')
      } catch (error) {
        if (error instanceof ZodError) {
          // Handle Zod validation errors
          const fieldErrors = {};
          error.errors.forEach((validationError) => {
            const fieldName = validationError.path.join(".");
            fieldErrors[fieldName] = validationError.message;
          });
          setFormErrors({ ...fieldErrors });
        }
      }      
    } else {
      setFormErrors({
        ...formErrors,
        confirm_password:"Passwords do not match"
      });
    }
  };

  return (
    <div className="w-[100vw] h-[100vh] flex relative">
      <div
        className={`w-full md:w-1/2 h-full flex flex-col justify-center items-center`}
      >
        <div className={`${isMobile?'w-[80%]':"w-[60%]"} flex flex-col items-center`}>
          {isRegistered ? (
            <div className="mt-[2rem] w-full flex flex-col items-center">
              <h2 className="text-primary font-semibold text-large">Success</h2>
              <p className="text-primary font-thin">
                Your account has been created.
              </p>
              <div className="w-full flex justify-center mt-[2rem]">
                <Button
                  className="w-[7rem]"
                  onClick={() => router.push("/login")}
                >
                  Go To Login
                </Button>
              </div>
            </div>
          ) : (
            <>
              <p
                className={`text-primary font-bold ${isMobile?'text-large':'text-extralarge'} md:text-large`}
              >
                Register
              </p>
              <p className="text-small text-primary font-light ">
                Please enter your details to get started
              </p>
              {formErrors.country_code && (<p className="text-xs pl-1 text-red-600 mt-[1rem]">{formErrors.country_code}</p>)}
              {formErrors.existing_user && (<p className="text-xs pl-1 text-red-600 mt-[1rem]">{formErrors.existing_user}</p>)}

              <Input
                type="text"
                className="w-full h-[4rem] mt-[2rem]"
                placeholder="Name"
                value={formData.name}
                noLabel
                error={formErrors.name}
                onChange={(e) =>
                  setFormData({ ...formData, name: e.target.value })
                }
              />
              <Input
                type="email"
                className="w-full h-[4rem]"
                placeholder="Email"
                value={formData.email}
                noLabel
                error={formErrors.email}
                onChange={(e) =>
                  setFormData({ ...formData, email: e.target.value })
                }
              />
               <div className="flex w-full gap-1"><Input
                type="string"
                className="w-[15%] h-[4rem]"
                placeholder="+XX"
                value={formData.country_code}
                noLabel
                onChange={(e) =>
                  setFormData({ ...formData, country_code: e.target.value })
                }
              /><Input
                type="number"
                className="w-[85%] h-[4rem]"
                placeholder="Contact"
                value={formData.phone_number}
                noLabel
                error={formErrors.phone_number}
                onChange={(e) =>
                  setFormData({ ...formData, phone_number: e.target.value })
                }
              /></div> 
              <Input
                type="password"
                className="w-full h-[4rem]"
                placeholder="Password"
                value={formData.password}
                noLabel
                error={formErrors.password}
                onChange={(e) =>
                  setFormData({ ...formData, password: e.target.value })
                }
              />
              <Input
                type="password"
                className="w-full h-[4rem]"
                placeholder="Confirm Password"
                value={formData.confirm_password}
                noLabel
                error={formErrors.confirm_password}
                onChange={(e) =>
                  setFormData({ ...formData, confirm_password: e.target.value })
                }
              />

              <div className="w-full flex justify-center">
                <Button className="w-[7rem]" onClick={onSubmit}>
                  Register
                </Button>
              </div>
            </>
          )}
        </div>
      </div>
      <div className="w-1/2 h-full hidden md:block  top-0 absolute right-0">
        <Image alt="login" src="/login.png" fill 
        sizes='50vw'
        placeholder="blur"
        blurDataURL={'/logo.png'}/>
      </div>
    </div>
  );
}
