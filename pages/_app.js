import "@/styles/globals.css";
import { Open_Sans } from "@next/font/google";
import Head from "next/head";
import Layout from "@/components/Layout";
import { SidebarProvider } from "@/components/SidebarContext";

const openSans = Open_Sans({
  subsets: ["latin"],
  display: "swap",
  weight: ["400", "600"], // Specify the desired font weights
});

export default function App({ Component, pageProps }) {
  return (
    <main className={openSans.className}>
      <Head>
        <title>Crest</title>
      </Head>
      <SidebarProvider>
        <Layout>
          <Component {...pageProps} />
        </Layout>
      </SidebarProvider>
    </main>
  );
}
