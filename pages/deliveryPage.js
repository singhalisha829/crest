import { BsPersonCircle } from 'react-icons/bs'
import { useMediaQuery } from 'react-responsive'
import Image from 'next/image'
import deliveryPageImage from "../public/deliveryPage.png"

export default function DeliveryPage() {
    const isMobile = useMediaQuery({ maxWidth: 767 }, undefined, (matches) => {
        // This callback is only executed on the client side
        return matches;
      })

    return(
    <div className="flex flex-col">
    <div className={`py-3 gap-4 items-center px-6 flex border-b-2 rounded-md w-[90%] mx-auto border-b-primary bg-white ${isMobile ? "mt-[1rem]" : "mt-[3rem]"}`}
      style={{ boxShadow: "2px 3px 6px 0px rgba(0, 82, 255, 0.25)" }}>
      <BsPersonCircle size={40} className="text-primary" />
      <span className="flex flex-col">
        <p className={`text-primary ${isMobile ? "text-semimedium" : "text-medium"} font-bold`}>Delivery Page</p>
        <p className="text-darkGrey text-extrasmall">Delivery Page</p>
      </span>
    </div>
    <div className='flex justify-center mt-[2rem]'>
                    <Image src={deliveryPageImage} placeholder='blur' alt='ornate logo' height={300} />
                </div>
    <p className={`${isMobile?"text-medium":"text-large"} font-bold mx-auto text-textCharcoalGrey`}>Congratulations</p>
    <p className={`${isMobile?"text-extrasmall":"text-small"} w-[80%] text-center mx-auto mt-2 text-textDarkGrey`}>You have completed all the procedures now our consultant will schedule a call with you (within 4-5 days) to discuss the information provided and ask for any additional or missing information.</p>
    </div>)
}