import { useEffect, useState } from "react";
import Input from "@/components/InputField";
import { SingleSelectDropdown } from "@/components/Dropdown";
import { BsPersonCircle } from "react-icons/bs";
import FormFooter from "@/components/FormFooter";
import { useRouter } from "next/router";
import { useMediaQuery } from "react-responsive";
import { z, ZodError } from "zod";
import { axiosInstance, masterDataInstance } from "@/services/ApiHandler";
import Button from "@/components/Button"
import LocalStorageService from "@/services/LocalStorageHandler";
import { useSidebar } from "@/components/SidebarContext";
import { PropagateLoader } from "react-spinners";

const localStorage = LocalStorageService.getService()

export default function Home() {
  const router = useRouter();
  const { toggleDisplayPageVisibility } = useSidebar();
  const [countryList, setCountryList] = useState([]);
  const [nationalityList, setNationalityList] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [personalDetails, setPersonalDetails] = useState({
    name: "",
    gender: "",
    date_of_birth: "",
    nationality: "",
    mobile_no: "",
    country: "",
    email: "",
    country_code:""
  });
  const [formErrors, setFormErrors] = useState({
    name: "",
    nationality: "",
    mobile_no: "",
    country: "",
    email: "",
  });
  const [isDisplayMode, setIsDisplayMode] = useState(false);
  const [isFormSubmitted,setIsFormSubmitted] = useState(false);
  const genderList = [
    { name: "Male", id: 1 },
    { name: "Female", id: 2 },
    { name: "Others", id: 3 },
  ];
  const [allPrevFormsFilled,setAllPrevFormsFilled] = useState({
    is_professional_details:'',
    is_personal_details:'',
    is_current_compensation_details_1:'',
    is_current_compensation_details_2:'',
    is_current_compensation_details_3:''
  });

  const isMobile = useMediaQuery({ maxWidth: 767 }, undefined, (matches) => {
    // This callback is only executed on the client side
    return matches;
  });

  // Define a schema
  const userSchema = z.object({
    // name: z.string().min(3),
    // email: z.string().email().or(z.literal('')),
    // mobile_no: z.number().int().positive().min(9),
    nationality: z.string().min(1),
    country: z.string().min(1),
  });

  useEffect(() => {
    if(localStorage.getAccessToken()){
      masterDataInstance
      .get("country/")
      .then((res) =>{
        if(res?.data.status.code == 200){
         setCountryList([...res.data.data.output])}});
    masterDataInstance
      .get("nationality/")
      .then((res) => {
        if(res?.data.status.code == 200){
          setNationalityList([...res.data.data.output])}});

    axiosInstance.get("client/").then((res) => {
      if(res?.data.status.code == 200){
      const details = res.data.data.output[0];
      if (details.is_personal_details) {
        setIsDisplayMode(true);
        setIsFormSubmitted(true)
      }
      setPersonalDetails({
        ...personalDetails,
        name: details.name,
        mobile_no: details.mobile_no,
        email: details.email,
        nationality: details.nationality,
        gender: details.gender,
        country: details.country,
        date_of_birth: details.date_of_birth,
        country_code:details.country_code
      });

      setAllPrevFormsFilled({
        is_professional_details:details.is_professional_details,
        is_personal_details:details.is_personal_details,
        is_current_compensation_details_1:details.is_current_compensation_details_1,
        is_current_compensation_details_2:details.is_current_compensation_details_2,
        is_current_compensation_details_3:details.is_current_compensation_details_3
      })

      if(details.is_current_compensation_details_1 && details.is_current_compensation_details_2 && details.is_current_compensation_details_3
        && details.is_personal_details && details.is_professional_details){
            toggleDisplayPageVisibility(true)
        }
        setIsLoading(false)
      }
    });
    }else{
      router.push('/login')
    }
   
  }, []);

  const onSubmit = () => {
    try {
      const validatedData = userSchema.parse(personalDetails);
      setFormErrors({
        name: "",
        nationality: "",
        mobile_no: "",
        country: "",
        email: "",
      });
      axiosInstance
        .put("client/", {
          nationality: personalDetails.nationality,
          country: personalDetails.country,
          date_of_birth: personalDetails.date_of_birth,
          gender: personalDetails.gender,
          is_personal_details: true,
        })
        .then((res) => {
          if (res.data.status.code == 200) {
            if(allPrevFormsFilled.is_current_compensation_details_2 && allPrevFormsFilled.is_current_compensation_details_3 && 
              allPrevFormsFilled.is_current_compensation_details_1 && allPrevFormsFilled.is_professional_details){
                  router.push('/deliveryPage');
                  toggleDisplayPageVisibility(true);
              }else{
            router.push("/professionalDetails");
              }
          }
        }).catch(error=>{
          if(error.response.status == 500){
              router.push('/500')
          }
      });

    } catch (error) {
      // console.error("Validation Error:", error.errors);
      if (error instanceof ZodError) {
        // Handle Zod validation errors
        const fieldErrors = {};
        error.errors.forEach((validationError) => {
          const fieldName = validationError.path.join(".");
          fieldErrors[fieldName] = validationError.message;
        });
        setFormErrors({ ...fieldErrors });
      }
    }
  };

  if (isLoading){
      return (
        <div className='row-span-full col-span-full flex justify-center items-center'>
          <PropagateLoader
            color='#01287A'
            loading={true}
            size={20}
            cssOverride={{top:'50vh',left:'-20px'}}
            />
        </div>
      )}
  return (
    <div className="flex flex-col">
      <div
        className={`py-3 gap-4 items-center px-6 flex border-b-2 rounded-md w-[90%] mx-auto border-b-primary bg-white ${
          isMobile ? "mt-[1rem]" : "mt-[3rem]"
        }`}
        style={{ boxShadow: "2px 3px 6px 0px rgba(0, 82, 255, 0.25)" }}
      >
        <BsPersonCircle size={40} className="text-primary" />
        <span className="flex flex-col">
          <p
            className={`text-primary ${
              isMobile ? "text-semimedium" : "text-medium"
            } font-bold`}
          >
            Personal Details
          </p>
          <p className="text-darkGrey text-extrasmall">Personal Details</p>
        </span>
      </div>

      {isFormSubmitted && (<div className={`w-[90%] mx-auto flex justify-end ${isMobile?"mt-5":"mt-7"} `}>{isDisplayMode? (
        <Button variant="transparent" className={`${isMobile?'text-xs w-[10rem] h-[1.7rem]':'w-[12rem] '} `} onClick={()=>setIsDisplayMode(false)}>
          Edit Personal Details
        </Button>
      ) : (
        <Button variant="transparent" className={`${isMobile?'text-xs w-[5rem] h-[1.7rem]':'w-[7rem] '} `} onClick={()=>setIsDisplayMode(true)}>
          Cancel Edit
        </Button>
      )}</div>)}

      {/* Personal Details form starts here */}
      <div className="w-[90%] mx-auto flex flex-wrap gap-[5%] mt-5">
        {/* Name(required): */}
        <Input
          type="text"
          className={`${isMobile ? "w-full mb-6" : "w-[47.5%] mb-[2rem]"}`}
          placeholder="* Enter your name"
          value={personalDetails.name}
          isDisplayMode={isDisplayMode}
          disabled
          onChange={(e) =>
            setPersonalDetails({
              ...personalDetails,
              name: e.target.value,
            })
          }
          error={formErrors.name}
        />
        {/* Gender: */}
        <SingleSelectDropdown
          options={genderList}
          optionName="name"
          optionID="id"
          placeholder="Gender"
          isDisplayMode={isDisplayMode}
          setselected={(name, id) => {
            setPersonalDetails({
              ...personalDetails,
              gender: name,
            });
          }}
          selected={personalDetails.gender}
          selectedOptionId={personalDetails.gender}
          className={`rounded-md ${
            isMobile ? "w-full mb-6" : "w-[47.5%] mb-[2rem]"
          }`}
        />
        {/* Date: */}
        <Input
          type="date"
          className={`${isMobile ? "w-full mb-6" : "w-[47.5%] mb-[2rem]"}`}
          placeholder="Date Of Birth"
          value={personalDetails.date_of_birth}
          isDisplayMode={isDisplayMode}
          onChange={(e) =>
            setPersonalDetails({
              ...personalDetails,
              date_of_birth: e.target.value,
            })
          }
        />
        {/* Nationality(required): */}
        <SingleSelectDropdown
          options={nationalityList}
          optionName="name"
          optionID="id"
          isDisplayMode={isDisplayMode}
          placeholder="* Nationality"
          setselected={(name, id) => {
            setPersonalDetails({
              ...personalDetails,
              nationality: name,
            });
          }}
          selected={personalDetails.nationality}
          selectedOptionId={personalDetails.nationality}
          error={formErrors.nationality}
          className={`rounded-md ${
            isMobile ? "w-full mb-6" : "w-[47.5%] mb-[2rem]"
          }`}
        />

        {/* Mobile No(required): */}
        <Input
          type="text"
          placeholder="* Mobile No."
          className={`${isMobile ? "w-full mb-6" : "w-[47.5%] mb-[2rem]"}`}
          value={personalDetails.country_code+" "+personalDetails.mobile_no}
          isDisplayMode={isDisplayMode}
          disabled
          onChange={(e) =>
            // setPersonalDetails({
            //   ...personalDetails,
            //   mobile_no: e.target.value,
            // })
            {}
          }
          error={formErrors.mobile_no}
        />

        {/* Country(required): */}
        <SingleSelectDropdown
          options={countryList}
          optionName="name"
          optionID="id"
          placeholder="* Country"
          isDisplayMode={isDisplayMode}
          setselected={(name, id) => {
            setPersonalDetails({
              ...personalDetails,
              country: name,
            });
          }}
          selected={personalDetails.country}
          selectedOptionId={personalDetails.country}
          error={formErrors.country}
          className={`rounded-md ${
            isMobile ? "w-full mb-6" : "w-[47.5%] mb-[2rem]"
          }`}
        />

        {/* Email: */}
        <Input
          type="email"
          className={`${isMobile ? "w-full mb-6" : "w-[47.5%] mb-[2rem]"}`}
          placeholder="Email address (Optional)"
          value={personalDetails.email}
          disabled
          isDisplayMode={isDisplayMode}
          onChange={(e) =>
            setPersonalDetails({
              ...personalDetails,
              email: e.target.value,
            })
          }
          error={formErrors.email}
        />
      </div>

      {/* Component to handle Back and Next Button */}
      {!isDisplayMode?<FormFooter
        className={`w-[90%] mx-auto  ${isMobile ? "mt-5" : "mt-[3rem]"}`}
        onNextClick={() => onSubmit()}
        noBackButton
      />:null}
    </div>
  );
}
