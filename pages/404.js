import { useRouter } from "next/router";
import Button from "@/components/Button";
import { useMediaQuery } from 'react-responsive'

export default function _404() {
    const router = useRouter();
    const isMobile = useMediaQuery({ maxWidth: 767 }, undefined, (matches) => {
        // This callback is only executed on the client side
        return matches;
      })

    return (
        <div className="flex col-span-full row-span-full h-full justify-center items-center">
            <div className={`${isMobile?"ml-[2rem]":"ml-[3rem]"}`}>
                <h1 className="text-[7rem] mb-[1rem] font-extrabold text-lightGrey">404 :/</h1>
                <h2 className={`${isMobile?"text-semimedium":"text-medium"} mb-3`}>Oops! This Page Could Not Be Found</h2>
                <p className={`mb-8 ${isMobile?"text-extrasmall":"text-small"}`}>
                SORRY BUT THE PAGE YOU ARE LOOKING FOR DOES NOT EXIST, HAVE BEEN REMOVED, NAME CHANGED OR IS TEMPORARILY UNAVAILABLE.
                </p>
                <span>
                    <Button
                        className="w-[15rem]"
                        onClick={() => {
                            router.push("/");
                        }}
                    >GO TO HOMEPAGE</Button>
                </span>
            </div>
        </div>
    );
}
