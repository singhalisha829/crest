import { useState, useEffect } from "react";
import Input from "@/components/InputField";
import { SingleSelectDropdown } from "@/components/Dropdown";
import { BsPersonCircle } from "react-icons/bs";
import FormFooter from "@/components/FormFooter";
import { useRouter } from "next/router";
import { Steps } from "antd";
import { useMediaQuery } from "react-responsive";
import { axiosInstance, masterDataInstance } from "@/services/ApiHandler";
import { z, ZodError } from "zod";
import Button from "@/components/Button";
import { useSidebar } from "@/components/SidebarContext";
import LocalStorageService from "@/services/LocalStorageHandler";
import { PropagateLoader } from "react-spinners";

const localStorage = LocalStorageService.getService();

export default function CurrentCompensation() {
  const router = useRouter();
  const { toggleDisplayPageVisibility } = useSidebar();
  const [classOfTravelList, setClassOfTravelList] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [
    currentCompensationDetailsSection1,
    setCurrentCompensationDetailsSection1,
  ] = useState({
    basic_salary_per_month: "",
    guaranteed_allowances_per_month: "",
    housing: "",
    transport: "",
    others: "",
  });
  const [sectionOneFormErrors, setSectionOneFormErrors] = useState({
    basic_salary_per_month: "",
    guaranteed_allowances_per_month: "",
    housing: "",
    transport: "",
    others: "",
  });
  const [
    currentCompensationDetailsSection2,
    setCurrentCompensationDetailsSection2,
  ] = useState({
    annual_bonus_opportunity: "",
    last_annual_bonus: "",
    last_drawn_long_term_incentives: "",
  });
  const [sectionTwoFormErrors, setSectionTwoFormErrors] = useState({
    annual_bonus_opportunity: "",
    last_annual_bonus: "",
    last_drawn_long_term_incentives: "",
  });
  const [
    currentCompensationDetailsSection3,
    setCurrentCompensationDetailsSection3,
  ] = useState({
    health_insurance: "",
    health_insurance_name: "",
    annual_vacation_days: "",
    life_insurance: "",
    life_insurance_name: "",
    air_ticket: "",
    air_ticket_name: "",
    life_insurance_annual_cover: "",
    air_ticket_annual_value: null,
    class_of_travel: "",
    car: "",
    car_name: "",
    gratuity_paid: "",
    gratuity_paid_name: "",
    fully_paid_and_maintained_by_company: null,
    fully_paid_and_maintained_by_company_name: "",
    gratuity_details_of_calculation: "",
    child_education_benefits: "",
    child_education_benefits_name: "",
    child_education_annual_value: "",
    health_insurance_benefits:[]
  });
  const [sectionThreeFormErrors, setSectionThreeFormErrors] = useState({
    air_ticket_name: "",
    life_insurance_annual_cover: "",
    class_of_travel: "",
    gratuity_details_of_calculation: "",
    child_education_annual_value: "",
  });
  const [healthInsuranceBenefitsData,setHealtInsuranceBenefitsData]= useState({
    health_insurance_annual_cover:'',
    worldwide_cover:'',
    dependents_cover:'',
    dental_cover:'',
    optical_cover:'',
    employee_contribution_towards_premium:'',
    deductible_per_visit:'',
    accidental_benefit:'',
    long_term_disability_benefit:''
  })
  const [isSection1FormSubmitted, setIsSection1FormSubmitted] = useState(false);
  const [isSection2FormSubmitted, setIsSection2FormSubmitted] = useState(false);
  const [isSection3FormSubmitted, setIsSection3FormSubmitted] = useState(false);
  const [isSection1DisplayMode, setIsSection1DisplayMode] = useState(false);
  const [isSection2DisplayMode, setIsSection2DisplayMode] = useState(false);
  const [isSection3DisplayMode, setIsSection3DisplayMode] = useState(false);
  const [allPrevFormsFilled, setAllPrevFormsFilled] = useState({
    is_professional_details: "",
    is_personal_details: "",
    is_current_compensation_details_1: "",
    is_current_compensation_details_2: "",
    is_current_compensation_details_3: "",
  });

  const dropdownOptions = [
    { name: "Yes", id: true },
    { name: "No", id: false },
  ];


  const [current, setCurrent] = useState(0);

  // Define a schema
  const userSchemaForSection1 = z.object({
    basic_salary_per_month: z
      .string()
      .min(1, { message: "This field is required." }),
    guaranteed_allowances_per_month: z
      .string()
      .min(1, { message: "This field is required." }),
    housing: z.string().min(1, { message: "This field is required." }),
    transport: z.string().min(1, { message: "This field is required." }),
    others: z.string().min(1, { message: "This field is required." }),
  });

  const userSchemaForSection2 = z.object({
    annual_bonus_opportunity: z
      .string()
      .min(1, { message: "This field is required." }),
    last_annual_bonus: z
      .string()
      .min(1, { message: "This field is required." }),
    last_drawn_long_term_incentives: z
      .string()
      .min(1, { message: "This field is required." }),
  });

  const userSchemaForSection3 = z.object({
    air_ticket_name: z.string().min(1),
    life_insurance_annual_cover: z
      .string()
      .refine(
        (value) => {
          // Check if the field should be compulsory based on 'responsible_for_any_revenue_or_cost_budgets'
          if (currentCompensationDetailsSection3.life_insurance) {
            return value !== undefined && value !== null && value !== "";
          }
          // If 'responsible_for_any_revenue_or_cost_budgets' is false, consider the field as valid
          return true;
        },
        { message: "This field is required." }
      )
      .optional(), // Marking optional

    class_of_travel: z
      .string()
      .refine(
        (value) => {
          // Check if the field should be compulsory
          if (currentCompensationDetailsSection3.air_ticket) {
            return value !== undefined && value !== null && value !== "";
          }
          // If false, consider the field as valid
          return true;
        },
        { message: "This field is required." }
      )
      .optional(), // Marking optional

    gratuity_details_of_calculation: z
      .string()
      .refine(
        (value) => {
          // Check if the field should be compulsory
          if (currentCompensationDetailsSection3.gratuity_paid) {
            return value !== undefined && value !== null && value !== "";
          }
          // If false, consider the field as valid
          return true;
        },
        { message: "This field is required." }
      )
      .optional(), // Marking optional

    child_education_annual_value: z
      .string()
      .refine(
        (value) => {
          // Check if the field should be compulsory
          if (currentCompensationDetailsSection3.child_education_benefits) {
            return value !== undefined && value !== null && value !== "";
          }
          // If false, consider the field as valid
          return true;
        },
        { message: "This field is required." }
      )
      .optional(), // Marking optional
  });

  const isMobile = useMediaQuery({ maxWidth: 767 }, undefined, (matches) => {
    // This callback is only executed on the client side
    return matches;
  });


  useEffect(() => {
    if (localStorage.getAccessToken()) {
      masterDataInstance
        .get("class-of-travel/")
        .then((res) => {
          if(res?.data.status.code == 200){
            setClassOfTravelList([...res.data.data.output])}});

      fetchCompensationDetails();
    } else {
      router.push("/login");
    }
  }, []);

  const fetchCompensationDetails = () => {
    axiosInstance.get("client/").then((res) => {
      if(res?.data.status.code == 200){
      const details = res.data.data.output[0];
      if (details.is_current_compensation_details_1) {
        setIsSection1DisplayMode(true);
        setIsSection1FormSubmitted(true);

        setCurrentCompensationDetailsSection1({
          basic_salary_per_month: details.basic_salary_per_month.toString(),
          guaranteed_allowances_per_month:
            details.guaranteed_allowances_per_month.toString(),
          housing: details.housing.toString(),
          transport: details.transport.toString(),
          others: details.others.toString(),
        });
      }
      if (details.is_current_compensation_details_2) {
        setIsSection2DisplayMode(true);
        setIsSection2FormSubmitted(true);

        setCurrentCompensationDetailsSection2({
          annual_bonus_opportunity: details.annual_bonus_opportunity.toString(),
          last_annual_bonus: details.last_annual_bonus.toString(),
          last_drawn_long_term_incentives:
            details.last_drawn_long_term_incentives.toString(),
        });
      }
      if (details.is_current_compensation_details_3) {
        setIsSection3DisplayMode(true);
        setIsSection3FormSubmitted(true);

        setCurrentCompensationDetailsSection3({
          health_insurance: details.health_insurance,
          health_insurance_name: getDisplayValue(details.health_insurance),
          annual_vacation_days: details.annual_vacation_days,
          life_insurance: details.life_insurance,
          life_insurance_name: getDisplayValue(details.life_insurance),
          air_ticket: details.air_ticket,
          air_ticket_name: getDisplayValue(details.air_ticket),
          life_insurance_annual_cover: details.life_insurance_annual_cover,
          class_of_travel: details.class_of_travel,
          car: details.car,
          car_name: getDisplayValue(details.car),
          gratuity_paid: details.gratuity_paid,
          gratuity_paid_name: getDisplayValue(details.gratuity_paid),
          fully_paid_and_maintained_by_company:details.fully_paid_and_maintained_by_company,
          fully_paid_and_maintained_by_company_name:getDisplayValue(details.fully_paid_and_maintained_by_company),
          gratuity_details_of_calculation:
            details.gratuity_details_of_calculation,
          child_education_benefits: details.child_education_benefits,
          child_education_benefits_name: getDisplayValue(details.child_education_benefits),
          child_education_annual_value: details.child_education_annual_value,
          air_ticket_annual_value: details.air_ticket_annual_value,
        });

        assignHealthInsuranceBenefitsValue(details.health_insurance_benefits);
      }

      setAllPrevFormsFilled({
        is_professional_details: details.is_professional_details,
        is_personal_details: details.is_personal_details,
        is_current_compensation_details_1:
          details.is_current_compensation_details_1,
        is_current_compensation_details_2:
          details.is_current_compensation_details_2,
        is_current_compensation_details_3:
          details.is_current_compensation_details_3,
      });

      if (
        details.is_current_compensation_details_1 &&
        details.is_current_compensation_details_2 &&
        details.is_current_compensation_details_3 &&
        details.is_personal_details &&
        details.is_professional_details
      ) {
        toggleDisplayPageVisibility(true);
      }

      setIsLoading(false);
    }
    });
  };

  const getDisplayValue = (data) =>{
    if(data != null){
      if(data){
        return 'Yes'
      }else{
        return 'No'
      }
    }
    return null
  }

  const assignHealthInsuranceBenefitsValue = (data) =>{
    let list=healthInsuranceBenefitsData
    data.map(benefits=>{
      list[benefits.question] = benefits.answer
    })
    setHealtInsuranceBenefitsData(list)
  }

  const onChange = (value) => {
    setCurrent(value);
  };

  const validateDataForSection1 = () => {
    try {
      const validatedData = userSchemaForSection1.parse(
        currentCompensationDetailsSection1
      );
      setSectionOneFormErrors({
        basic_salary_per_month: "",
        guaranteed_allowances_per_month: "",
        housing: "",
        transport: "",
        others: "",
      });

      axiosInstance
        .put("client/", {
          ...currentCompensationDetailsSection1,
          is_current_compensation_details_1: true,
        })
        .then((res) => {
          if (res.data.status.code == 200) {
            if (
              allPrevFormsFilled.is_current_compensation_details_2 &&
              allPrevFormsFilled.is_current_compensation_details_3 &&
              allPrevFormsFilled.is_personal_details &&
              allPrevFormsFilled.is_professional_details
            ) {
              router.push("/deliveryPage");
              toggleDisplayPageVisibility(true);
            } else {
              setCurrent(1);
              fetchCompensationDetails();
              setIsSection1DisplayMode(true);
            }
          }
        })
        .catch((error) => {
          if (error.response.status == 500) {
            router.push("/500");
          }
        });
    } catch (error) {
      // console.error('Validation Error:', error.errors);
      if (error instanceof ZodError) {
        // Handle Zod validation errors
        const fieldErrors = {};
        error.errors.forEach((validationError) => {
          const fieldName = validationError.path.join(".");
          fieldErrors[fieldName] = validationError.message;
        });
        setSectionOneFormErrors({ ...fieldErrors });
      }
    }
  };

  const validateDataForSection2 = () => {
    try {
      const validatedData = userSchemaForSection2.parse(
        currentCompensationDetailsSection2
      );
      setSectionTwoFormErrors({
        annual_bonus_opportunity: "",
        last_annual_bonus: "",
        last_drawn_long_term_incentives: "",
      });

      axiosInstance
        .put("client/", {
          ...currentCompensationDetailsSection2,
          is_current_compensation_details_2: true,
        })
        .then((res) => {
          if (res.data.status.code == 200) {
            if (
              allPrevFormsFilled.is_current_compensation_details_1 &&
              allPrevFormsFilled.is_current_compensation_details_3 &&
              allPrevFormsFilled.is_personal_details &&
              allPrevFormsFilled.is_professional_details
            ) {
              router.push("/deliveryPage");
              toggleDisplayPageVisibility(true);
            } else {
              setCurrent(2);
              fetchCompensationDetails()
              setIsSection2DisplayMode(true);
            }
          }
        })
        .catch((error) => {
          if (error.response.status == 500) {
            router.push("/500");
          }
        });
    } catch (error) {
      // console.error('Validation Error:', error.errors);
      if (error instanceof ZodError) {
        // Handle Zod validation errors
        const fieldErrors = {};
        error.errors.forEach((validationError) => {
          const fieldName = validationError.path.join(".");
          fieldErrors[fieldName] = validationError.message;
        });
        setSectionTwoFormErrors({ ...fieldErrors });
      }
    }
  };

  const validateDataForSection3 = () => {
    try {
      const validatedData = userSchemaForSection3.parse(
        currentCompensationDetailsSection3
      );
      setSectionThreeFormErrors({
        air_ticket_name: "",
        life_insurance_annual_cover: "",
        class_of_travel: "",
        gratuity_details_of_calculation: "",
        child_education_annual_value: "",
      });

      axiosInstance
        .put("client/", {
          ...currentCompensationDetailsSection3,
          is_current_compensation_details_3: true,
        })
        .then((res) => {
          if (res.data.status.code == 200) {
            if (
              allPrevFormsFilled.is_current_compensation_details_2 &&
              allPrevFormsFilled.is_current_compensation_details_3 &&
              allPrevFormsFilled.is_personal_details &&
              allPrevFormsFilled.is_professional_details
            ) {
              router.push("/deliveryPage");
              toggleDisplayPageVisibility(true);
            } else {
              fetchCompensationDetails();
              setIsSection3DisplayMode(true);
            }
          }
        })
        .catch((error) => {
          if (error.response.status == 500) {
            router.push("/500");
          }
        });
    } catch (error) {
      // console.error("Validation Error:", error.errors);
      if (error instanceof ZodError) {
        // Handle Zod validation errors
        const fieldErrors = {};
        error.errors.forEach((validationError) => {
          const fieldName = validationError.path.join(".");
          fieldErrors[fieldName] = validationError.message;
        });
        setSectionThreeFormErrors({ ...fieldErrors });
      }
    }
  };

  const handleHealthInsuranceBenefits=(key,value)=>{
    let questionArray = currentCompensationDetailsSection3.health_insurance_benefits;
    const existingQuestionIndex = questionArray.findIndex(item => item.question === key);
    if(existingQuestionIndex == -1){
      questionArray.push({question:key,answer:value})
    }else{
      questionArray[existingQuestionIndex].answer=value
    }
    setCurrentCompensationDetailsSection3({...currentCompensationDetailsSection3,health_insurance_benefits:questionArray})
    setHealtInsuranceBenefitsData({...healthInsuranceBenefitsData,[key]:value})
  }


  const sectionOne = (
    <>
      {isSection1FormSubmitted && (
        <div className="w-[90%] mx-auto flex justify-end mr">
          {isSection1DisplayMode ? (
            <Button
              variant="transparent"
              className={`${
                isMobile ? "text-xs w-[11rem] h-[1.7rem]" : "w-[13rem] "
              } `}
              onClick={() => setIsSection1DisplayMode(false)}
            >
              Edit Compensation Details
            </Button>
          ) : (
            <Button
              variant="transparent"
              className={`${
                isMobile ? "text-xs w-[5rem] h-[1.7rem]" : "w-[7rem] "
              } `}
              onClick={() => setIsSection1DisplayMode(true)}
            >
              Cancel Edit
            </Button>
          )}
        </div>
      )}
      <div className="w-full flex flex-wrap gap-[5%] mt-4">
        {/* Basic Salary(required): */}
        <Input
          type="number"
          className={`${isMobile ? "w-[98%] mb-6" : "w-[47.5%] mb-[2rem]"}`}
          placeholder="* Basic Salary (per month in USD)"
          value={currentCompensationDetailsSection1.basic_salary_per_month}
          onChange={(e) =>{if (/^\d*\.?\d*$/.test(e.target.value)) {
            setCurrentCompensationDetailsSection1({
              ...currentCompensationDetailsSection1,
              basic_salary_per_month: e.target.value,
            })}}
          }
          labelCss="font-normal"
          isDisplayMode={isSection1DisplayMode}
          error={sectionOneFormErrors.basic_salary_per_month}
        />

        {/* Guaranteed Allowances(required): */}
        <Input
          type="number"
          className={`${isMobile ? "w-[98%] mb-6" : "w-[47.5%] mb-[2rem]"}`}
          placeholder="* Guaranteed Allowances (per month in USD)"
          value={
            currentCompensationDetailsSection1.guaranteed_allowances_per_month
          }
          labelCss="font-normal"
          onChange={(e) =>{if (/^\d*\.?\d*$/.test(e.target.value)) {
            setCurrentCompensationDetailsSection1({
              ...currentCompensationDetailsSection1,
              guaranteed_allowances_per_month: e.target.value,
            })}}
          }
          isDisplayMode={isSection1DisplayMode}
          error={sectionOneFormErrors.guaranteed_allowances_per_month}
        />

        {/* Housing(required): */}
        <Input
          type="number"
          className={`${isMobile ? "w-[98%] mb-6" : "w-[47.5%] mb-[2rem]"}`}
          placeholder="* Housing (per month in USD)"
          value={currentCompensationDetailsSection1.housing}
          onChange={(e) =>{if (/^\d*\.?\d*$/.test(e.target.value)) {
            setCurrentCompensationDetailsSection1({
              ...currentCompensationDetailsSection1,
              housing: e.target.value,
            })}}
          }
          isDisplayMode={isSection1DisplayMode}
          error={sectionOneFormErrors.housing}
          labelCss="font-normal"
        />

        {/* Transport(required): */}
        <Input
          type="number"
          className={`${isMobile ? "w-[98%] mb-6" : "w-[47.5%] mb-[2rem]"}`}
          placeholder="* Transport (per month in USD)"
          value={currentCompensationDetailsSection1.transport}
          onChange={(e) =>{if (/^\d*\.?\d*$/.test(e.target.value)) {
            setCurrentCompensationDetailsSection1({
              ...currentCompensationDetailsSection1,
              transport: e.target.value,
            })}}
          }
          isDisplayMode={isSection1DisplayMode}
          error={sectionOneFormErrors.transport}
          labelCss="font-normal"
        />

        {/* Others(required): */}
        <Input
          type="number"
          className={`${isMobile ? "w-[98%] mb-6" : "w-[47.5%] mb-[2rem]"}`}
          placeholder="* Others (per month in USD)"
          value={currentCompensationDetailsSection1.others}
          onChange={(e) =>{if (/^\d*\.?\d*$/.test(e.target.value)) {
            setCurrentCompensationDetailsSection1({
              ...currentCompensationDetailsSection1,
              others: e.target.value,
            })}}
          }
          labelCss="font-normal"
          isDisplayMode={isSection1DisplayMode}
          error={sectionOneFormErrors.others}
        />

        {/* Email Address:
      <Input
        type="email"
        className={`${isMobile ? "w-[98%] mb-6" : "w-[47.5%] mb-[2rem]"}`}
        placeholder="Email address (Optional)"
        value={currentCompensationDetailsSection1.current_compensation_email}
        onChange={(e) =>
          setCurrentCompensationDetailsSection1({
            ...currentCompensationDetailsSection1,
            current_compensation_email: e.target.value,
          })
        }
        isDisplayMode={isSection1DisplayMode}
        labelCss='font-normal'
      /> */}
      </div>
      {!isSection1DisplayMode ? (
        <FormFooter
          className={`w-full mx-auto h-[2rem]`}
          onNextClick={() => validateDataForSection1()}
          noBackButton
        />
      ) : null}
    </>
  );

  const sectionTwo = (
    <>
      {isSection2FormSubmitted && (
        <div className="w-[90%] mx-auto flex justify-end mr">
          {isSection2DisplayMode ? (
            <Button
              variant="transparent"
              className={`${
                isMobile ? "text-xs w-[11rem] h-[1.7rem]" : "w-[13rem] "
              } `}
              onClick={() => setIsSection2DisplayMode(false)}
            >
              Edit Compensation Details
            </Button>
          ) : (
            <Button
              variant="transparent"
              className={`${
                isMobile ? "text-xs w-[5rem] h-[1.7rem]" : "w-[7rem] "
              } `}
              onClick={() => setIsSection2DisplayMode(true)}
            >
              Cancel Edit
            </Button>
          )}
        </div>
      )}
      <div className="w-full flex flex-wrap gap-[5%] mt-4">
        {/* Annual Bonus Oppurtunity(required): */}
        <Input
          type="number"
          className={`${isMobile ? "w-[98%] mb-6" : "w-[47.5%] mb-[2rem]"}`}
          placeholder="* Annual Bonus opportunity @ 100% (in USD)"
          value={currentCompensationDetailsSection2.annual_bonus_opportunity}
          onChange={(e) =>{if (/^\d*\.?\d*$/.test(e.target.value)) {
            setCurrentCompensationDetailsSection2({
              ...currentCompensationDetailsSection2,
              annual_bonus_opportunity: e.target.value,
            })}}
          }
          labelCss="font-normal"
          error={sectionTwoFormErrors.annual_bonus_opportunity}
          isDisplayMode={isSection2DisplayMode}
        />

        {/* Last Annual Bonus(required): */}
        <Input
          type="number"
          className={`${isMobile ? "w-[98%] mb-6" : "w-[47.5%] mb-[2rem]"}`}
          placeholder="* Last annual bonus (in USD)"
          value={currentCompensationDetailsSection2.last_annual_bonus}
          onChange={(e) =>{if (/^\d*\.?\d*$/.test(e.target.value)) {
            setCurrentCompensationDetailsSection2({
              ...currentCompensationDetailsSection2,
              last_annual_bonus: e.target.value,
            })}}
          }
          labelCss="font-normal"
          error={sectionTwoFormErrors.last_annual_bonus}
          isDisplayMode={isSection2DisplayMode}
        />

        {/* Last Drawn Incentive(required): */}
        <Input
          type="number"
          className={`${isMobile ? "w-[98%] mb-6" : "w-[47.5%] mb-[2rem]"}`}
          placeholder="* Last drawn Long term incentives (in USD)"
          value={
            currentCompensationDetailsSection2.last_drawn_long_term_incentives
          }
          onChange={(e) =>{if (/^\d*\.?\d*$/.test(e.target.value)) {
            setCurrentCompensationDetailsSection2({
              ...currentCompensationDetailsSection2,
              last_drawn_long_term_incentives: e.target.value,
            })}}
          }
          labelCss="font-normal"
          isDisplayMode={isSection2DisplayMode}
          error={sectionTwoFormErrors.last_drawn_long_term_incentives}
        />
      </div>
      {!isSection2DisplayMode ? (
        <FormFooter
          className={`w-full mx-auto h-[2rem]`}
          onNextClick={() => validateDataForSection2()}
          onBackClick={() => setCurrent(0)}
        />
      ) : null}
    </>
  );

  const sectionThree = (
    <>
      {isSection3FormSubmitted && (
        <div className="w-[90%] mx-auto flex justify-end mr">
          {isSection3DisplayMode ? (
            <Button
              variant="transparent"
              className={`${
                isMobile ? "text-xs w-[11rem] h-[1.7rem]" : "w-[12rem] "
              } `}
              onClick={() => setIsSection3DisplayMode(false)}
            >
              Edit Compensation Details
            </Button>
          ) : (
            <Button
              variant="transparent"
              className={`${
                isMobile ? "text-xs w-[5rem] h-[1.7rem]" : "w-[7rem] "
              } `}
              onClick={() => setIsSection3DisplayMode(true)}
            >
              Cancel Edit
            </Button>
          )}
        </div>
      )}
      <div className="w-full flex flex-wrap gap-[5%] mt-4">
        {/* Health Insurance: */}
        <SingleSelectDropdown
          options={dropdownOptions}
          optionName="name"
          optionID="id"
          placeholder="Health Insurance"
          setselected={(name, id) => {
            if(!id){
              setHealtInsuranceBenefitsData({
                health_insurance_annual_cover:'',
                worldwide_cover:'',
                dependents_cover:'',
                dental_cover:'',
                optical_cover:'',
                employee_contribution_towards_premium:'',
                deductible_per_visit:'',
                accidental_benefit:'',
                long_term_disability_benefit:''
              })
              setCurrentCompensationDetailsSection3({
                ...currentCompensationDetailsSection3,
                health_insurance: id,
                health_insurance_name: name,
                health_insurance_benefits:[]
              });
            }else{
            setCurrentCompensationDetailsSection3({
              ...currentCompensationDetailsSection3,
              health_insurance: id,
              health_insurance_name: name,
            });
          }
          }}
          selected={currentCompensationDetailsSection3.health_insurance_name}
          selectedOptionId={currentCompensationDetailsSection3.health_insurance}
          labelCss="font-normal"
          className={`rounded-md ${
            isMobile ? "w-[98%] mb-6" : "w-[47.5%] mb-[2rem]"
          }`}
          isDisplayMode={isSection3DisplayMode}
        />

        {/* Health Insurance Benefits: */}
        {currentCompensationDetailsSection3.health_insurance ? (
          <div
            className={`rounded-md ${
              isMobile ? "w-[98%] mb-6" : "w-[47.5%] mb-[2rem]"
            }`}
          >
            <label className={`mb-4 text-xs text-darkGrey font-normal`}>
              Health Insurance Benefits
            </label>
            <div className="grid grid-cols-2 gap-3 w-full mt-2 items-end">
              {/* Annual Cover (in USD): */}
              <Input
                type="number"
                placeholder="Annual Cover (in USD)"
                labelCss="font-normal"
                value={
                  healthInsuranceBenefitsData.health_insurance_annual_cover
                }
                error=''
                onChange={(e) =>{if (/^\d*\.?\d*$/.test(e.target.value)) {handleHealthInsuranceBenefits("health_insurance_annual_cover",e.target.value)}}}
                isDisplayMode={isSection3DisplayMode}
              />

              {/* Worldwide Cover: */}
              <SingleSelectDropdown
                options={dropdownOptions}
                optionName="name"
                optionID="id"
                placeholder="Worldwide Cover"
                setselected={(name, id) =>handleHealthInsuranceBenefits("worldwide_cover",name)}
                selected={
                  healthInsuranceBenefitsData.worldwide_cover
                }
                labelCss="font-normal"
                isDisplayMode={isSection3DisplayMode}
              />

                {/* Dependents Cover: */}
              <SingleSelectDropdown
                options={dropdownOptions}
                optionName="name"
                optionID="id"
                placeholder="Dependents Cover"
                setselected={(name, id) => handleHealthInsuranceBenefits("dependents_cover",name)}
                selected={
                  healthInsuranceBenefitsData.dependents_cover
                }
                labelCss="font-normal"
                isDisplayMode={isSection3DisplayMode}
              />

              {/* Dental Cover: */}
              <SingleSelectDropdown
                options={dropdownOptions}
                optionName="name"
                optionID="id"
                placeholder="Dental Cover"
                setselected={(name, id) =>handleHealthInsuranceBenefits("dental_cover",name)}
                selected={
                  healthInsuranceBenefitsData.dental_cover
                }
                labelCss="font-normal"
                isDisplayMode={isSection3DisplayMode}
              />

                {/* Optical Cover: */}
              <SingleSelectDropdown
                options={dropdownOptions}
                optionName="name"
                optionID="id"
                placeholder="Optical Cover"
                setselected={(name, id) => handleHealthInsuranceBenefits("optical_cover",name)}
                selected={
                  healthInsuranceBenefitsData.optical_cover
                }
                labelCss="font-normal"
                isDisplayMode={isSection3DisplayMode}
              />

                {/* Employee contribution towards premium (%): */}
              <Input
                type="number"
                placeholder="Employee contribution towards premium (%)"
                labelCss="font-normal"
                value={healthInsuranceBenefitsData.employee_contribution_towards_premium}
                error=''
                onChange={(e) =>{if (/^\d*\.?\d*$/.test(e.target.value)) {handleHealthInsuranceBenefits("employee_contribution_towards_premium",e.target.value)}}}
                isDisplayMode={isSection3DisplayMode}
              />

              {/* Deductible per visit (in USD): */}
              <Input
                type="number"
                placeholder="Deductible per visit (in USD)"
                labelCss="font-normal"
                error=''
                value={healthInsuranceBenefitsData.deductible_per_visit}
                onChange={(e) =>{if (/^\d*\.?\d*$/.test(e.target.value)) {handleHealthInsuranceBenefits("deductible_per_visit",e.target.value)}}}
                isDisplayMode={isSection3DisplayMode}
              />

              {/* Accident Benefit */}
              <SingleSelectDropdown
                options={dropdownOptions}
                optionName="name"
                optionID="id"
                placeholder="Accident Benefit"
                setselected={(name, id) => handleHealthInsuranceBenefits("accidental_benefit",name)}
                selected={
                  healthInsuranceBenefitsData.accidental_benefit
                }
                labelCss="font-normal"
                isDisplayMode={isSection3DisplayMode}
              />

                {/* Long Term Disability benefit */}
              <SingleSelectDropdown
                options={dropdownOptions}
                optionName="name"
                optionID="id"
                placeholder="Long Term Disability benefit"
                setselected={(name, id) => handleHealthInsuranceBenefits("long_term_disability_benefit",name)}
                selected={
                  healthInsuranceBenefitsData.long_term_disability_benefit
                }
                labelCss="font-normal"
                isDisplayMode={isSection3DisplayMode}
              />
            </div>
          </div>
        ) : (
          <br />
        )}

        {/* Life Insurance: */}
        <SingleSelectDropdown
          options={dropdownOptions}
          optionName="name"
          optionID="id"
          placeholder="Life Insurance"
          setselected={(name, id) => {
            if(!id){
              setCurrentCompensationDetailsSection3({
                ...currentCompensationDetailsSection3,
                life_insurance: id,
                life_insurance_name: name,
                life_insurance_annual_cover:''
              })
            }else{
            setCurrentCompensationDetailsSection3({
              ...currentCompensationDetailsSection3,
              life_insurance: id,
              life_insurance_name: name,
            });
          }
          }}
          selected={currentCompensationDetailsSection3.life_insurance_name}
          selectedOptionId={currentCompensationDetailsSection3.life_insurance}
          labelCss="font-normal"
          className={`rounded-md ${
            isMobile ? "w-[98%] mb-6" : "w-[47.5%] mb-[2rem]"
          }`}
          isDisplayMode={isSection3DisplayMode}
        />

        {/* Life Insurance Cover(required): */}
        {currentCompensationDetailsSection3.life_insurance ? (
          <Input
            type="number"
            className={`${isMobile ? "w-[98%] mb-6" : "w-[47.5%] mb-[2rem]"}`}
            placeholder="* Life Insurance annual cover (in USD)"
            value={
              currentCompensationDetailsSection3.life_insurance_annual_cover
            }
            onChange={(e) =>{if (/^\d*\.?\d*$/.test(e.target.value)) {
              setCurrentCompensationDetailsSection3({
                ...currentCompensationDetailsSection3,
                life_insurance_annual_cover: e.target.value,
              })}}
            }
            error={sectionThreeFormErrors.life_insurance_annual_cover}
            labelCss="font-normal"
            isDisplayMode={isSection3DisplayMode}
          />
        ) : (
          <br />
        )}

        {/* Air ticket(required): */}
        <SingleSelectDropdown
          options={dropdownOptions}
          optionName="name"
          optionID="id"
          placeholder="* Annual Vacation Air ticket"
          setselected={(name, id) => {
            if(!id){
              setCurrentCompensationDetailsSection3({
                ...currentCompensationDetailsSection3,
                air_ticket: id,
                air_ticket_name: name,
                air_ticket_annual_value:null,
                class_of_travel:''
              });
            }else{
            setCurrentCompensationDetailsSection3({
              ...currentCompensationDetailsSection3,
              air_ticket: id,
              air_ticket_name: name,
            });
          }
          }}
          selected={currentCompensationDetailsSection3.air_ticket_name}
          selectedOptionId={currentCompensationDetailsSection3.air_ticket}
          error={sectionThreeFormErrors.air_ticket_name}
          labelCss="font-normal"
          className={`rounded-md ${
            isMobile ? "w-[98%] mb-6" : "w-[47.5%] mb-[2rem]"
          }`}
          isDisplayMode={isSection3DisplayMode}
        />

        {currentCompensationDetailsSection3.air_ticket ? (
          <>
            {/* Air Ticket Annual Cover for entire family: */}
            <Input
              type="number"
              className={`${isMobile ? "w-[98%] mb-6" : "w-[47.5%] mb-[2rem]"}`}
              placeholder="* Air Ticket Annual Cover for entire family (in USD)"
              value={currentCompensationDetailsSection3.air_ticket_annual_value}
              onChange={(e) =>{if (/^\d*\.?\d*$/.test(e.target.value)) {
                setCurrentCompensationDetailsSection3({
                  ...currentCompensationDetailsSection3,
                  air_ticket_annual_value: e.target.value,
                })}}
              }
              labelCss="font-normal"
              isDisplayMode={isSection3DisplayMode}
            />

            {/* Class of Travel(required): */}
            <SingleSelectDropdown
              options={classOfTravelList}
              optionName="name"
              optionID="id"
              placeholder="* Class of Travel"
              setselected={(name, id) => {
                setCurrentCompensationDetailsSection3({
                  ...currentCompensationDetailsSection3,
                  class_of_travel: name,
                });
              }}
              selected={currentCompensationDetailsSection3.class_of_travel}
              selectedOptionId={
                currentCompensationDetailsSection3.class_of_travel
              }
              error={sectionThreeFormErrors.class_of_travel}
              className={`rounded-md ${
                isMobile ? "w-[98%] mb-6" : "w-[47.5%] mb-[2rem]"
              }`}
              labelCss="font-normal"
              isDisplayMode={isSection3DisplayMode}
            />
            <br />
          </>
        ) : (
          <br />
        )}

        {/* Car: */}
        <SingleSelectDropdown
          options={dropdownOptions}
          optionName="name"
          optionID="id"
          placeholder="Car"
          setselected={(name, id) => {
            if(!id){
              setCurrentCompensationDetailsSection3({
                ...currentCompensationDetailsSection3,
                car: id,
                car_name: name,
                fully_paid_and_maintained_by_company:null,
                fully_paid_and_maintained_by_company_name:''
              });
            }else{
            setCurrentCompensationDetailsSection3({
              ...currentCompensationDetailsSection3,
              car: id,
              car_name: name,
            });
          }
          }}
          selected={currentCompensationDetailsSection3.car_name}
          selectedOptionId={currentCompensationDetailsSection3.car}
          error={sectionThreeFormErrors.car_name}
          labelCss="font-normal"
          className={`rounded-md ${
            isMobile ? "w-[98%] mb-6" : "w-[47.5%] mb-[2rem]"
          }`}
          isDisplayMode={isSection3DisplayMode}
        />

        {/* Fully paid and maintained by company?: */}
        {currentCompensationDetailsSection3.car ? (
          <SingleSelectDropdown
            options={dropdownOptions}
            optionName="name"
            optionID="id"
            placeholder="Fully paid and maintained by company?"
            setselected={(name, id) => {
              setCurrentCompensationDetailsSection3({
                ...currentCompensationDetailsSection3,
                fully_paid_and_maintained_by_company: id,
                fully_paid_and_maintained_by_company_name: name,
              });
            }}
            selected={
              currentCompensationDetailsSection3.fully_paid_and_maintained_by_company_name
            }
            selectedOptionId={
              currentCompensationDetailsSection3.fully_paid_and_maintained_by_company
            }
            labelCss="font-normal"
            className={`rounded-md ${
              isMobile ? "w-[98%] mb-6" : "w-[47.5%] mb-[2rem]"
            }`}
            isDisplayMode={isSection3DisplayMode}
          />
        ) : (
          <br />
        )}

        {/* Gratuity: */}
        <SingleSelectDropdown
          options={dropdownOptions}
          optionName="name"
          optionID="id"
          placeholder="Gratuity paid"
          setselected={(name, id) => {
            if(!id){
              setCurrentCompensationDetailsSection3({
                ...currentCompensationDetailsSection3,
                gratuity_paid: id,
                gratuity_paid_name: name,
                gratuity_details_of_calculation:''
              });
            }else{
              setCurrentCompensationDetailsSection3({
                ...currentCompensationDetailsSection3,
                gratuity_paid: id,
                gratuity_paid_name: name,
              });
            }
            
          }}
          selected={currentCompensationDetailsSection3.gratuity_paid_name}
          selectedOptionId={currentCompensationDetailsSection3.gratuity_paid}
          labelCss="font-normal"
          className={`rounded-md ${
            isMobile ? "w-[98%] mb-6" : "w-[47.5%] mb-[2rem]"
          }`}
          isDisplayMode={isSection3DisplayMode}
        />

        {/* Gratuity details(required): */}
        {currentCompensationDetailsSection3.gratuity_paid ? (
          <Input
            type="textarea"
            rows={6}
            className={`${isMobile ? "w-[98%] mb-6" : "w-[47.5%] mb-[2rem]"}`}
            placeholder="* Gratuity details of calculation"
            value={
              currentCompensationDetailsSection3.gratuity_details_of_calculation
            }
            onChange={(e) =>
              setCurrentCompensationDetailsSection3({
                ...currentCompensationDetailsSection3,
                gratuity_details_of_calculation: e.target.value,
              })
            }
            labelCss="font-normal"
            error={sectionThreeFormErrors.gratuity_details_of_calculation}
            isDisplayMode={isSection3DisplayMode}
          />
        ) : (
          <br />
        )}

        {/* Child Education Benefits: */}
        <SingleSelectDropdown
          options={dropdownOptions}
          optionName="name"
          optionID="id"
          placeholder="Child education benefits"
          setselected={(name, id) => {
            if(!id){
              setCurrentCompensationDetailsSection3({
                ...currentCompensationDetailsSection3,
                child_education_benefits: id,
                child_education_benefits_name: name,
                child_education_annual_value:''
              });
            }else{
              setCurrentCompensationDetailsSection3({
                ...currentCompensationDetailsSection3,
                child_education_benefits: id,
                child_education_benefits_name: name,
              });
            }
         
          }}
          selected={
            currentCompensationDetailsSection3.child_education_benefits_name
          }
          selectedOptionId={
            currentCompensationDetailsSection3.child_education_benefits
          }
          labelCss="font-normal"
          className={`rounded-md ${
            isMobile ? "w-[98%] mb-6" : "w-[47.5%] mb-[2rem]"
          }`}
          isDisplayMode={isSection3DisplayMode}
        />

        {/* Child education annual value: */}
        {currentCompensationDetailsSection3.child_education_benefits ? (
          <Input
            type="number"
            className={`${isMobile ? "w-[98%] mb-6" : "w-[47.5%] mb-[2rem]"}`}
            placeholder="* Child education annual value (in USD)"
            value={
              currentCompensationDetailsSection3.child_education_annual_value
            }
            onChange={(e) =>{if (/^\d*\.?\d*$/.test(e.target.value)) {
              setCurrentCompensationDetailsSection3({
                ...currentCompensationDetailsSection3,
                child_education_annual_value: e.target.value,
              })}}
            }
            labelCss="font-normal"
            error={sectionThreeFormErrors.child_education_annual_value}
            isDisplayMode={isSection3DisplayMode}
          />
        ) : null}
      </div>
      {!isSection3DisplayMode ? (
        <FormFooter
          className={`w-full mx-auto h-[2rem]`}
          onNextClick={() => validateDataForSection3()}
          onBackClick={() => setCurrent(1)}
        />
      ) : (
        <br />
      )}
    </>
  );

  if (isLoading) {
    return (
      <div className="row-span-full col-span-full flex justify-center items-center">
        <PropagateLoader
          color="#01287A"
          loading={true}
          size={20}
          cssOverride={{ top: "50vh", left: "-20px" }}
        />
      </div>
    );
  }
  return (
    <div className="flex flex-col overflow-scroll h-full relative">
      <div
        className={` gap-4 py-3 items-center px-6 flex border-b-2 rounded-md w-[90%] mx-auto border-b-primary bg-white ${
          isMobile ? "mt-[1rem]" : "mt-[3rem]"
        }`}
        style={{ boxShadow: "2px 3px 6px 0px rgba(0, 82, 255, 0.25)" }}
      >
        <BsPersonCircle size={40} className="text-primary" />
        <span className="flex flex-col">
          <p
            className={`text-primary ${
              isMobile ? "text-semimedium" : "text-medium"
            } font-bold`}
          >
            Current Compensation
          </p>
          <p className="text-darkGrey text-extrasmall">Current Compensation</p>
        </span>
      </div>

      {/* Current compensation form starts here.. */}
      <div className="w-[90%] mx-auto flex flex-wrap gap-[5%] mt-[3rem]">
        <Steps
          direction="vertical"
          size="small"
          current={current}
          onChange={onChange}
          items={[
            {
              title: "Your Current Compensation",
              description: current == 0 ? sectionOne : null,
            },
            {
              title: "Total Guaranteed monthly allowances",
              description: current == 1 ? sectionTwo : null,
            },
            {
              title: "Your Benefits",
              description: current == 2 ? sectionThree : null,
            },
          ]}
        />
      </div>
    </div>
  );
}
