import React, { useState, useRef, useEffect } from 'react'
import { MdKeyboardArrowDown, MdKeyboardArrowUp  } from 'react-icons/md'
//Assets
import { RiCheckboxCircleFill } from 'react-icons/ri'
import { MdOutlineClose, MdSearch } from 'react-icons/md';


/* Multiple Select Dropdown */
export function MultiSelectDropdown({
  options,
  optionName,
  optionID,
  selected,
  setselected,
  activeOptionIndex,
  required,
  color,
  padding,
  label,
  placeholder,
  disabled,
  className,
  ...props
}) {
  const [isOpen, setIsOpen] = useState(false)
  const [localOptions, setLocalOptions] = useState()
  const [selectedOption, setSelectedOption] = useState(selected)
  const [searchString, setSearchString] = useState('')
  let selectedOptionsLength = selected.length
  const clickRef = useRef()
  const toggling = () => setIsOpen(!isOpen)
  const filterOptions = (e) => {
    let filteredOptions = options.filter((option) =>
      option[optionName].toLowerCase().includes(e.target.value.toLowerCase())
    )
    if (filteredOptions.length > 0) {
      setLocalOptions(filteredOptions)
    } else {
      setLocalOptions([])
    }
  }
  const handleSelect = async (e, option) => {
    e.stopPropagation()

    const found = selectedOption.find(
      (s) => s[optionName] === option[optionName]
    )
    if (found) {
      const newSelected = selectedOption.filter(
        (s) => s[optionName] !== option[optionName]
      )
      setSelectedOption(newSelected)
      setselected(newSelected)

      return
    }
    const existSelection = [...selected, option]
    setSelectedOption(existSelection)
    setselected(existSelection)
  }

  const closeDropdown = (e) => {
    setIsOpen(false)
  }
  // Track events outside scope
  const clickOutside = (e) => {
    if (clickRef.current.contains(e.target)) {
      return
    }
    // outside click
    setIsOpen(false)
  }
  useEffect(() => {
    //reset local options
    setLocalOptions(options)
    //cleaning last searched
    setSearchString('')
    if (isOpen) {
      document.addEventListener('mousedown', clickOutside)
    }
    return () => {
      document.removeEventListener('mousedown', clickOutside)
    }
  }, [selectedOption, isOpen])

  return (
    <div
      className={`${selectedOptionsLength != 0?'border-softviolet':''} ${
        disabled
          ? `dropdown bg-primaryHighlight cursor-not-allowed border-b-lightGrey shadow-none `
          : `dropdown `
      } ${className}`}
      ref={clickRef}
    >
      <div
        onClick={toggling}
        style={{ padding: padding, color: color }}
        className={`innerDiv cursor-pointer multipleSelectInnerDiv `}
      >

        {isOpen ? (
          <input
            onChange={(e) => {
              filterOptions(e)
              setSearchString(e.target.value)
              /* setSelectedOption(e.target.value); */
            }}
            placeholder={placeholder}
            value={searchString}
            required={required}
            autoFocus
            disabled={disabled}
            className={`ml-2 `}
          />
        ) : (
          <div className='selectedOptionList'>
            {selected.length == 0 ? (
              <input className='w-full' placeholder={placeholder}/>
            ) : (
              <p className={`${selectedOptionsLength != 0?'text-softviolet':''}`} title={selected.map((item) => item[optionName])}>
                {selected[selectedOptionsLength - 1][optionName]}{' '}
                {selectedOptionsLength > 1 && `+ ${selected.length - 1}`}
              </p>
            )}
          </div>
        )}
        <div className='selectedOptionList'>
          {selected.length>0?<span
            onClick={(e) => {
              e.stopPropagation()
              setselected([])
              setSelectedOption([])
              props.onFilterCancel()
            }}
          >
            <MdOutlineClose title='Cancel' size={20} color="#757575"/>
            <MdSearch size={20} title="Search" color='#757575' onClick={(e)=>{props.handleDropdownSelections(); e.stopPropagation()}} />

          </span>:null}

        </div>
      </div>{' '}
      {!isOpen && (
        <ul
          onMouseLeave={() => closeDropdown()}
          style={{
            listStyle: 'none',
            padding: '0',
          }}
          className='ul top-[2.2rem] left-0 rounded-md overflow-hidden'
        >
          {localOptions !== undefined && localOptions.length > 0 ? (
            localOptions.map((option, index) => {
              const isSelected = selectedOption.find(
                (s) => s[optionName] === option[optionName]
              )
              return (
                <li onClick={(e) => handleSelect(e, option)} key={index}>
                  <div className='multiselectOption'>
                    {isSelected ? (
                      <p className='selectedText'>
                        <RiCheckboxCircleFill color='var(--blue)' />
                      </p>
                    ) : (
                      <p className='selectedText'>
                        <RiCheckboxCircleFill />
                      </p>
                    )}
                    <p className={`m-0`}
                    style={{color:option.color}}>{option[optionName]}</p>
                  </div>
                </li>
              )
            })
          ) : (
            <li style={{ cursor: 'no-drop' }}>
              <div className='multiselectOption'>
                <p className='m-0 dark:text-white'>No options available</p>
              </div>
            </li>
          )}
        </ul>
      )}
    </div>
  )
}


export function SingleSelectDropdown({
  options,
  optionName,
  optionID,
  selected,
  setselected,
  activeOptionIndex,
  required,
  padding,
  label,
  placeholder,
  disabled,
  className,
  isDisplayMode,
  labelCss,
  ...props
}) {
  const [isOpen, setIsOpen] = useState(false)
  const [localOptions, setLocalOptions] = useState()
  const [selectedOption, setSelectedOption] = useState('')
  const inputRef = useRef();
  const singleSelectClickRef = useRef();

  const toggling = () => {
    if (!disabled) {
      if (isOpen) {
        inputRef.current.blur()
      } else {
        inputRef.current.focus()
      }

      setIsOpen(!isOpen)
    }
  }

  // Track events outside scope
  const clickOutside = (e) => {
    if (singleSelectClickRef.current.contains(e.target)) {
      return
    }
    // outside click
    setIsOpen(false)
  }

  const filterOptions = (e) => {
    let filteredOptions = options.filter((option) =>
      option[optionName].toLowerCase().includes(e.target.value.toLowerCase())
    )
    if (filteredOptions.length > 0) {
      setLocalOptions(filteredOptions)
    } else {
      setLocalOptions([])
      setSelectedOption(null)
    }
  }
  const onOptionClicked = (value, i) => {
    setSelectedOption(value[optionName])
    // activeOptionIndex(value[optionID])
    setselected(value[optionName], value[optionID])
    setIsOpen(false)
  }
  useEffect(() => {
    setLocalOptions(options)

    if (isOpen) {
      document.addEventListener('mousedown', clickOutside)
    }
    return () => {
      document.removeEventListener('mousedown', clickOutside)
    }
    setSelectedOption(selected)
  }, [isOpen, selected, options])

  return (
    <div className={`flex flex-col ${className}`} ref={singleSelectClickRef}>
      <label className={`mb-2 text-xs text-darkGrey ${labelCss?labelCss:'font-thin'}`} htmlFor={label}>
          {placeholder} :
        </label>
      {isDisplayMode?<span>{(selected != undefined && selected != '')?selected:'-'}</span>:<div
        className={` dropdown h-[2.25rem]  ${selectedOption != ''?'border-softviolet border-2':'border-1'} ${
          disabled ? 'bg-primaryHighlight cursor-not-allowed border-b-lightGrey shadow-none' : 'cursor-pointer'
        } 
        ${props.error?'border-red-500 border-b-2 shadow-md shadow-red-200 ':'border-b-2 border-b-primary'}`}
        style={props.error?{}:{boxShadow:"2px 3px 6px 0px rgba(0, 82, 255, 0.25)"}}
        onClick={toggling}
        >
          <input
            onChange={(e) => {
              filterOptions(e)
              setSelectedOption(e.target.value)
            }}
            placeholder={placeholder}
            value={selected}
            required={required}
            disabled={disabled}
            className={`w-full pl-2 relative h-full rounded-md placeholder:text-extrasmall
            transition-[border] focus-visible:border-l-[10px] focus-visible:border-primary focus-visible:outline-primary  
            ${disabled?"bg-disabledBlockColor cursor-not-allowed":"bg-white"}
            ${selectedOption != ''?'text-softviolet':''} `}
            ref={(input) => (inputRef.current = input)}
          />
          {isOpen ? (
            <MdKeyboardArrowUp className='absolute text-primary right-[0.5rem] top-[0.6rem]' />
          ) : (
            <MdKeyboardArrowDown className='absolute text-primary right-[0.5rem] top-[0.6rem]' />
          )}
        {' '}
        {isOpen && (
          <ul className={`ul p-2 w-full top-[2.2rem] left-0 rounded-md overflow-hidden mt-2`}>
            {localOptions !== undefined && localOptions.length > 0 ? (
              localOptions.map((option, i) => {
                let selectedStyle='';
                if(option[optionID] === props.selectedOptionId){
                  selectedStyle ='bg-lightviolet '
                }
                return (
                <li
                  className={`p-2 ${selectedStyle}`}
                  style={{color:option.color}}
                  onClick={() => onOptionClicked(option, i)}
                  key={i}
                >
                  {option[optionName]}
                </li>
              )})
            ) : (
              <li>No Options</li>
            )}
          </ul>
        )}
      </div>}
      {/* display this text when required field is empty */}
      {props.error?<p className="text-xxsmall pl-1 text-red mt-1">This field is required.</p>:null}
    </div>
  )
}

