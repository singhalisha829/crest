import { useState } from "react";
import { MdVisibility, MdVisibilityOff } from "react-icons/md";

const Input = ({
  type,
  label,
  value,
  onChange,
  min,
  max,
  onBlur,
  readOnly,
  placeholder,
  radio,
  disabled,
  interviewRadioOption,
  onRadioOptionChange,
  labelCss,
  ...restProps
}) => {
  const [showPassword, setShowPassword] = useState(false);

  let displayData;

  if (type == "url") {
    displayData = (
      <span
        className={`cursor-pointer hover:underline`}
        onClick={() => window.open(value)}
      >
        {value}
      </span>
    );
  } else if (type == "file") {
    displayData = (
      <span
        className={`cursor-pointer hover:underline`}
        onClick={() => window.open(restProps.displayValue)}
      >
        Download
      </span>
    );
  } else {
    displayData = (
      <span>{value != undefined && value != "" ? value : "-"}</span>
    );
  }

  return (
    <div
      className={`flex relative flex-col ${
        restProps.className ? restProps.className : ""
      }`}
    >
      {!restProps.noLabel ? (
        <label
          className={`mb-2 text-xs text-darkGrey ${
            labelCss ? labelCss : "font-thin"
          }`}
          htmlFor={label}
        >
          {placeholder} :
        </label>
      ) : null}

      {restProps.isDisplayMode ? (
        displayData
      ) : (
        <>
          {type !== "textarea" &&
            type !== "password" && (
              <>
                <input
                  className={` px-2 h-[2.25rem] w-full placeholder:text-extrasmall text-sm rounded-md 
          ${disabled ? "bg-disabledBlockColor" : "bg-white"} ${
                    type == "file" ? "pt-[0.2rem]" : ""
                  }
          ${
            restProps.error
              ? "border-red-500 border-b-2 shadow-md shadow-red-200 "
              : "border-b-primary border-b-2"
          } 
           border-1  transition-[border] focus-visible:border-l-[10px] focus-visible:border-primary focus-visible:outline-primary`}
                  style={
                    restProps.error
                      ? {}
                      : { boxShadow: "2px 3px 6px 0px rgba(0, 82, 255, 0.25)" }
                  }
                  type={type}
                  id={label}
                  value={value}
                  onChange={onChange}
                  onBlur={onBlur}
                  placeholder={placeholder}
                  min={min}
                  accept={restProps.accept}
                  max={max}
                  disabled={disabled}
                  readOnly={readOnly}
                  onClick={() => (restProps.onClick ? restProps.onClick() : {})}
                />
                {/* display this text when required field is empty */}
                {restProps.error !== "" && !restProps.isDisplayMode ? (
                  <p className="text-xxsmall pl-1 text-red mt-1">
                    {restProps.error}
                  </p>
                ) : null}
              </>
            )}

          {/* password field */}
          {type === "password" && (
            <div className="relative">
              <input
                className={` px-2 h-[2.25rem] w-full placeholder:text-extrasmall text-sm rounded-md 
          ${disabled ? "bg-disabledBlockColor" : "bg-white"} ${
                  type == "file" ? "pt-[0.2rem]" : ""
                }
          ${
            restProps.error
              ? "border-red-500 border-b-2 shadow-md shadow-red-200 "
              : "border-b-primary border-b-2"
          } 
           border-1  transition-[border] focus-visible:border-l-[10px] focus-visible:border-primary focus-visible:outline-primary`}
                style={
                  restProps.error
                    ? {}
                    : { boxShadow: "2px 3px 6px 0px rgba(0, 82, 255, 0.25)" }
                }
                type={showPassword ? "text" : "password"}
                id={label}
                value={value}
                onChange={onChange}
                onBlur={onBlur}
                placeholder={placeholder}
                min={min}
                accept={restProps.accept}
                max={max}
                disabled={disabled}
                readOnly={readOnly}
                onClick={() => (restProps.onClick ? restProps.onClick() : {})}
              />
              {showPassword ? (
                <MdVisibilityOff
                  className="absolute top-[0.6rem] right-2 cursor-pointer"
                  onClick={() => setShowPassword(!showPassword)}
                />
              ) : (
                <MdVisibility
                  className="absolute top-[0.6rem] right-2 cursor-pointer"
                  onClick={() => setShowPassword(!showPassword)}
                />
              )}
              {/* display this text when required field is empty */}
              {restProps.error !== "" && !restProps.isDisplayMode ? (
                <p className="text-xxsmall pl-1 text-red mt-1">
                  {restProps.error}
                </p>
              ) : null}
            </div>
          )}

          {/* textarea field */}
          {type == "textarea" && (
            <>
              <textarea
                className={`px-2 text-sm rounded-md border-1 placeholder:text-extrasmall transition-[border] 
          ${disabled ? "bg-disabledBlockColor cursor-not-allowed" : "bg-white"}
          ${
            restProps.error
              ? "border-red-500 shadow-md border-b-2 shadow-red-200 "
              : "border-b-2 border-b-primary "
          } focus-visible:border-l-[10px] focus-visible:border-primary focus-visible:outline-primary`}
                style={
                  restProps.error
                    ? {}
                    : { boxShadow: "2px 3px 6px 0px rgba(0, 82, 255, 0.25)" }
                }
                id={label}
                rows={restProps.rows}
                onChange={onChange}
                onBlur={onBlur}
                placeholder={placeholder}
                min={min}
                max={max}
                disabled={disabled}
                readOnly={readOnly}
              >
                {value}
              </textarea>
              {/* display this text when required field is empty */}
              {restProps.error !== "" ? (
                <p className="text-xxsmall pl-1 text-red mt-1">
                  {restProps.error}
                </p>
              ) : null}
            </>
          )}
        </>
      )}
    </div>
  );
};

export default Input;
