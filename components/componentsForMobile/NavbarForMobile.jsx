import { BiMenu } from 'react-icons/bi';
import { useState, useEffect } from 'react';
import { FaTruck, FaSuitcase, FaRupeeSign, FaPowerOff } from "react-icons/fa";
import { IoNewspaperOutline, IoSettingsOutline } from 'react-icons/io5'
import {BsPersonCircle} from 'react-icons/bs'

import Image from 'next/image'
import Link from 'next/link'
import logo from '../../public/logo.png'
import { useRouter, usePathname } from 'next/navigation';
import LocalStorageService from '@/services/LocalStorageHandler'
import { useMediaQuery } from 'react-responsive'
import topRightCorner from "../../public/topright.svg"
import bottomLeftCorner from "../../public/bottomleft.svg"


const localStorage = LocalStorageService.getService()

const Navbar = ({ children }) => {
    const [isOpen, setIsOpen] = useState(false);
    const [userName, setUserName] = useState(null)
    const [userRole, setUserRole] = useState(null)
    const router = useRouter();
    const toggleMenu = () => {
        setIsOpen(!isOpen);
    };
    const currentUrl = usePathname()

    const isMobile = useMediaQuery({ maxWidth: 767 }, undefined, (matches) => {
        // This callback is only executed on the client side
        return matches;
      })

    useEffect(() => {
        const user = localStorage.getUser()
        if (user != null || user != undefined) {
            setUserName(user.name)
            setUserRole(user.role)
        }
    }, [])

    const logout = () => {
        localStorage.clearToken()
        router.push('/login')
    }


    return (
        <div className="wrapper">
            <div 
            style={isOpen?{marginLeft:"15rem",width:"calc(100% - 15rem",transition:"all 0.5s ease"}:{}}>
                <div className="flex items-center h-[3.5rem] p-2 border-b-1">
                <BiMenu size={30} className='hamburger cursor-pointer' onClick={()=>toggleMenu()} />              </div>

                <div>

                    {/* Mobile menu */}
                    {isOpen ? <div className={`fixed top-0 left-0 w-[15rem] h-full pt-[20px] flex flex-col gap-8 py-8 shadow-md basis-[15%]`}
                    style={{transition:"all 0.5s ease"}}>
                        {/* Logo */}
                        <div className='flex justify-center'>
                            <Image src={logo} placeholder='blur' alt='ornate logo' height={50}/>
                        </div>

                        <nav>
                            <ul className='flex flex-col font-light'>
                            {[
                            { name: 'Personal Details', path: '/', icon: <BsPersonCircle /> },
                            { name: 'Professional details', path: '/professionalDetails', icon: <FaSuitcase /> },
                            { name: 'Current Compensation', path: '/currentCompensation', icon: <IoNewspaperOutline /> },
                            { name: 'Delivery Page', path: '/deliveryPage', icon: <FaTruck /> },
                            // { name: 'Payment', path: '/payment', icon: <FaRupeeSign /> },
                        ].map((menuItem) => {
                                    return (
                                        <li key={menuItem.name} className='py-1'>
                                                <Link
                                                    className={`relative gap-1 py-2 pr-2 items-center flex
                                                    ${currentUrl == menuItem.path ? 'font-medium bg-primaryHighlight ' : ''} text-primary w-[98%] pl-4 cursor-pointer`}
                                                    href={
                                                        menuItem.path
                                                    }
                                                    onClick={() => setIsOpen(false)}
                                                >
                                                    <span className='text-l'>{menuItem.icon}</span>

                                                    <span className='flex justify-between w-full'
                                                    >
                                                        {menuItem.name}

                                                    </span>
                                                </Link>
                                        </li>
                                    )
                                })}
                            </ul>
                        </nav>

                        <div className='w-[80%] mx-auto border-b-2'/>

                        <nav>
                            <ul className='flex flex-col font-light'>
                            {[
                            // { name: 'Settings', path: '/settings', icon: <IoSettingsOutline /> },
                            { name: 'Log Out', path: '/login', icon: <FaPowerOff /> },
                        ].map((menuItem) => {
                                    return (
                                        <li key={menuItem.name} className='py-1'>
                                                <Link
                                                    className={`relative gap-1 py-2 pr-2 items-center flex
                                                    ${currentUrl == menuItem.path ? 'font-medium bg-primaryHighlight ' : ''} text-primary w-[98%] pl-4 cursor-pointer`}
                                                    href={
                                                        menuItem.path
                                                    }
                                                    onClick={() => setIsOpen(false)}
                                                >
                                                    <span className='text-l'>{menuItem.icon}</span>

                                                    <span className='flex justify-between w-full'
                                                    >
                                                        {menuItem.name}

                                                    </span>
                                                </Link>
                                        </li>
                                    )
                                })}
                            </ul>
                        </nav>

                       

                        
                    </div> : null}
                </div>
                {/* children pages */}
                <section className={`${isOpen ? 'left-[15rem] blur-sm' : ''} ${isMobile?'p-0 h-[calc(100vh-3.5rem)]':'p-2 h-[calc(100vh-4rem)]'}  overflow-scroll`}>
                <Image className='fixed top-[3.5rem] right-0 z-[-1]' src={topRightCorner} alt='top pattern' height={100}/>
            <Image className='fixed bottom-0 left-0 z-[-1]' src={bottomLeftCorner} alt='bottom pattern' height={100}/>

             {children}</section>
            </div>

        </div>
    )
}

export default Navbar;