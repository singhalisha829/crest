import Image from 'next/image'
import Link from 'next/link'
import { useRouter } from 'next/router'
import { usePathname } from 'next/navigation'
import logo from '../public/logo.png'
import LocalStorageService from '@/services/LocalStorageHandler'
import { FaTruck, FaSuitcase,  FaPowerOff } from "react-icons/fa";
import { IoNewspaperOutline } from 'react-icons/io5'
import {BsPersonCircle} from 'react-icons/bs'
import topRightCorner from "../public/topright.svg"
import bottomLeftCorner from "../public/bottomleft.svg"
import { useSidebar } from './SidebarContext'

const localStorage = LocalStorageService.getService()


export default function Sidebar({ children }) {    
    const router = useRouter()
    const currentUrl = usePathname();
    const {isDisplayPageVisible} = useSidebar();

    const logout = () => {
        localStorage.clearToken()
        router.push('/login')
    }
    return (
        <div className={`flex h-[100vh] relative`}>
            <div className='flex flex-col gap-[3rem] py-8 text-primary font-normal text-extrasmall basis-[15%] min-w-[16rem] max-w-[16rem] shadow-md'>
                {/* Logo */}
                <div className='flex justify-center'>
                    <Image src={logo} placeholder='blur' alt='crest logo' height={50} />
                </div>
                {/* Menu */}
                <nav>
                    <ul className='flex flex-col gap-2 font-light'>
                        {[
                            { name: 'Personal Details', path: '/', icon: <BsPersonCircle /> ,isVisible:true},
                            { name: 'Professional details', path: '/professionalDetails', icon: <FaSuitcase />,isVisible:true },
                            { name: 'Current Compensation', path: '/currentCompensation', icon: <IoNewspaperOutline />,isVisible:true },
                            { name: 'Delivery Page', path: '/deliveryPage', icon: <FaTruck />, isVisible:isDisplayPageVisible },
                            // { name: 'Payment', path: '/payment', icon: <FaRupeeSign /> },
                        ].map((menuItem) => {
                            return (
                                <li key={menuItem.name} style={{display:menuItem.isVisible?'block':'none'}}>
                                    <div className={`py-3 ${currentUrl == menuItem.path?'bg-primary rounded-r-full text-white w-[90%]':''}`}>
                                        <Link
                                            className={`relative flex gap-3 ml-4 items-center cursor-pointer `}
                                            href={
                                                menuItem.path
                                            }
                                        >
                                            <span className='text-l'>{menuItem.icon}</span>
                                            <span
                                            >
                                                {menuItem.name}
                                            </span>
                                        </Link>
                                    </div>
                                </li>
                            )
                        })}
                    </ul>
                </nav>

                <div className='w-[80%] mx-auto border-b-2'/>

                <nav>
                    <ul className='flex flex-col gap-2 font-light'>
                        {[
                            // { name: 'Settings', path: '/settings', icon: <IoSettingsOutline /> },
                            { name: 'Log Out', path: '/login', icon: <FaPowerOff /> },
                        ].map((menuItem) => {
                            return (
                                <li key={menuItem.name}>
                                    <div className={`py-3 ${currentUrl === menuItem.path?'bg-primary rounded-r-full text-white w-[90%]':''}`}>
                                        <Link
                                            className={`relative flex gap-3 ml-4 items-center cursor-pointer `}
                                            href={
                                                menuItem.path
                                            }
                                            onClick={()=>{
                                                if(menuItem.name=='Log Out'){
                                                    logout()
                                                }
                                            }}
                                        >
                                            <span className='text-l'>{menuItem.icon}</span>
                                            <span
                                            >
                                                {menuItem.name}
                                            </span>
                                        </Link>
                                    </div>
                                </li>
                            )
                        })}
                    </ul>
                </nav>

                </div>
            <section className=' basis-[85%] h-[100vh] overflow-scroll'>
            <Image className='fixed top-0 right-0 z-[-1]' src={topRightCorner} alt='top pattern' />
            <Image className='fixed bottom-0 left-[16rem] z-[-1]' src={bottomLeftCorner} alt='bottom pattern' />

                {children}</section>
        </div>
    )
}
