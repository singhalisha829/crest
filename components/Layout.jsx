import { usePathname } from 'next/navigation'
import { useMediaQuery } from 'react-responsive'
import dynamic from 'next/dynamic';

const Navbar = dynamic(() => import('./componentsForMobile/NavbarForMobile'), { ssr: false });
const Sidebar = dynamic(() => import('./Sidebar'), { ssr: false });

export default function Layout({ children }) {
  const currentUrl = usePathname()
  const isMobile = useMediaQuery({ maxWidth: 767 }, undefined, (matches) => {
    // This callback is only executed on the client side
    return matches;
  })


  return (
    <>{(currentUrl == '/login' || currentUrl == '/register') ? <main>{children}</main> : 
      isMobile?<Navbar>{children}</Navbar>:<Sidebar >{children}</Sidebar>
      }</>
  )
}
