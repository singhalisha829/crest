'use client'

const Button = ({
    children,
    className,
    size,
    variant,
    customText,
    onClick,
    disabled,
  }) => {
    let width
    let background
    switch (size) {
      case 'extrasmall':
        width = 'w-extrasmall'
        break
      case 'small':
        width = 'w-small'
        break
      case 'medium':
        width = 'w-medium'
        break
      case 'semimedium':
        width = 'w-semimedium'
        break
      case 'large':
        width = 'w-large'
        break
      default:
        width = 'fit-content'
    }
  
    switch (variant) {
      case 'primary':
        background = `${disabled ? 'bg-darkGrey text-white' : 'bg-primary text-white'}`
        break
      case 'transparent':
        background = `${disabled ? 'bg-darkGrey text-white' : 'bg-white text-darkGrey'}`
        break
      default:
        background = `${disabled ? 'bg-darkGrey text-white' : 'bg-primary text-white'}`
    }    

  
    return (
      <button
        className={`${className ? className : ''} ${width} ${background} font-semibold py-1 rounded-md`}
        style={{boxShadow:"3px 4px 8px 0px rgba(0, 0, 0, 0.25)"}}
        onClick={onClick}
        disabled={disabled}
      >
        {children}
      </button>
    )
  }
  
  export default Button
  