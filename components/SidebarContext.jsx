// contexts/SidebarContext.js
import React, { createContext, useState, useContext } from 'react';

const SidebarContext = createContext();

export const SidebarProvider = ({ children }) => {
  const [isDisplayPageVisible, setIsDisplayPageVisible] = useState(false);

  const toggleDisplayPageVisibility = (currentState) => {
    setIsDisplayPageVisible(currentState);
  };

  return (
    <SidebarContext.Provider value={{ isDisplayPageVisible, toggleDisplayPageVisibility }}>
      {children}
    </SidebarContext.Provider>
  );
};

export const useSidebar = () => useContext(SidebarContext);
