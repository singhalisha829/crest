import Button from "./Button";

const FormFooter = (props) =>{
    return(
        <div className={`flex justify-end ${props.className}`}>
             {!props.noBackButton?<Button
                      variant="transparent"
                      className=" w-[7rem] mr-4"
                      onClick={() => props.onBackClick()}
                    >
                      Back
                    </Button>:null}
                    <Button
                      disabled={props.disableNextButton}
                      className="w-[7rem]"
                      onClick={() => props.onNextClick()}
                    >
                      Next
                    </Button>
        </div>
    )
}

export default FormFooter;