import axios from 'axios'
import LocalStorageService from './LocalStorageHandler'

const localStorage = LocalStorageService.getService()

//Constants
const URL = process.env.SERVER

export const accessInstance = axios.create({
  baseURL: URL + 'api/accounts/',
})

export const axiosInstance = axios.create({
  baseURL: URL + 'api/accounts/',
})

export const masterDataInstance = axios.create({
  baseURL: URL + 'api/master/',
})
/*=========== Interceptor ================ */
axiosInstance.interceptors.request.use(
  (request) => {
    request.headers['Authorization'] = 'token ' + localStorage.getAccessToken()
    return request
  },
  (error) => {
    return Promise.reject(error)
  }
)

masterDataInstance.interceptors.request.use(
  (request) => {
    request.headers['Authorization'] = 'token ' + localStorage.getAccessToken()
    return request
  },
  (error) => {
    return Promise.reject(error)
  }
)


axiosInstance.interceptors.response.use(
  function (response) {
    if(response.data.status.code === 200){
      return response
    }else if(response.data.status.code === 500){
      window.location.href = '/500'
    }
  },
  //redirecting to login page on following error codes
  function (error) {
    if(error.response.status == 401 || error.response.status == 403){
      window.location.href = "/login";
    }
    return Promise.reject(error)
  }
)

masterDataInstance.interceptors.response.use(
  function (response) {
    if(response.data.status.code === 200){
      return response
    }else if(response.data.status.code === 500){
      window.location.href = '/500'
    }
  },
  //redirecting to login page on following error codes
  function (error) {
    if(error.response.status == 401 || error.response.status == 403){
      window.location.href = "/login";
    }
    return Promise.reject(error)
  }
)
