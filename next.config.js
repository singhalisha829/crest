/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  env: {
    SERVER: process.env.SERVER,
    baseURL: process.env.baseURL,
  },
}

module.exports = nextConfig
