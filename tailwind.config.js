/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    './pages/**/*.{js,ts,jsx,tsx,mdx}',
    './components/**/*.{js,ts,jsx,tsx,mdx}',
    './app/**/*.{js,ts,jsx,tsx,mdx}',
  ],
   theme: {
    extend: {
      fontFamily: {
        sans: ["var(--font-poppins)"],
      },
      fontWeight: {
        thin: 100,
        light: 300,
        regular: 400,
        semibold: 600,
        medium: 500,
        bold: 700,
      },
      colors: {
        darkGrey: "#616161",
        primary: "#01287A",
        primaryHighlight: "#DAE6FF",
        lightGrey: "#C0BEC2",
        textGrey:"#2A2A2A",
        textDarkGrey:"#373737",
        textCharcoalGrey:"#2D2D2D",
        disabledBlockColor:"#EBEBEB"
      },
      backgroundImage: {
        "orange-shade":
          "linear-gradient(81.53deg, #F37920 17.22%, #FF9E5E 86.25%)",
      },
      borderWidth: {
        1: "1px",
      },
      fontSize:{
        xxsmall:"10px",
        extrasmall: "13px",
        small:"15px",
        semimedium:"20px",
        medium:"24px",
        large:'30px',
        extralarge:"40px"
      },
      spacing: {
        extrasmall: "13px",
        small: "10rem",
        medium: "24px",
        semimedium: "14rem",
        large: "20rem",
      },
      gridTemplateRows: {
        layout: "repeat(36, minmax(0, 1fr))",
      },
      animation: {
        "spin-slow": "spin 1.5s linear infinite",
      },
    },
  },
  plugins: [],
}
